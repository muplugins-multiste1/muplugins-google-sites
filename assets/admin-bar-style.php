<?php
$referrer = wp_get_referer();
global $product_logo;
#wpadminbar
echo '<style type="text/css">

		@font-face {
			font-family: "LeagueSpartan";
			src: url("'. plugin_dir_url(__FILE__) . 'fonts/LeagueSpartan-Regular.ttf");
			font-weight: normal;
			font-style: normal;
		}
		
		@font-face {
			font-family: "LeagueSpartan Bold";
			src: url("'. plugin_dir_url(__FILE__) . 'fonts/LeagueSpartan-Bold.ttf");
			font-weight: bold;
			font-style: normal;
		}
		
		@font-face {
			font-family: "LeagueSpartan Light";
			src: url("'. plugin_dir_url(__FILE__) . 'fonts/LeagueSpartan-Light.ttf");
			font-weight: light;
			font-style: normal;
		}

		#adminmenu *, #wpadminbar *{
			font-family: "LeagueSpartan", sans-serif;
		}
		#adminmenu .wp-submenu-head, 
		#adminmenu a.menu-top{
			font-weight: lighter;
		}
		/*estilos admin menu*/
		#adminmenu, #adminmenu .wp-submenu, #adminmenuback, #adminmenuwrap {
			background-color: #fff!important;
		}
		#adminmenu li {
			border-bottom: 1px solid '. $secondary .';
		}
		#adminmenu li.wp-menu-separator {
			padding-top: 6px;
			margin: 0;
			border-bottom: 0;
		}
		#adminmenu li.wp-menu-separator .separator, {
			border-bottom: 1px solid '. $secondary .';
		}
		#adminmenu li:hover div.wp-menu-image:before,
		#adminmenu li a:hover {
			color: '. $primary .';
		}
		#adminmenu li.wp-not-current-submenu:hover > a,
		#adminmenu li:hover > a {
			background-color: '. $secondary .'!important;
		}
		#adminmenu li.wp-has-current-submenu div.wp-menu-image:before {
			color: '. $primary .'!important;
		}
		#adminmenu a,
		#adminmenu div.wp-menu-image:before {
			color: #262626!important;
		}
		#adminmenu a:hover,
		#adminmenu a:hover div.wp-menu-image:before,
		#adminmenu li:hover  {
			color: '. $primary .'!important;
		}
		#adminmenu .wp-has-current-submenu>a  {
			color: '. $primary .'!important;
			background-color: '. $secondary .'!important;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu {
			padding: 0!important;
			background-color: '. $secondary .'!important;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu li a {
			padding-top: 10px;
			padding-bottom: 10px;
			font-size: 14px !important;
		}
		ul#adminmenu a.wp-has-current-submenu:after,
		ul#adminmenu>li.current>a.current:after,
		ul#adminmenu a.wp-not-current-submenu:after {
			border-right-color: '. $primary .'!important;
		}
		#adminmenu li.wp-has-submenu.wp-not-current-submenu:hover:after {
			border-right-color: '. $primary .';
		}
		#adminmenu .wp-submenu li {
			border-bottom: 0;
		}
		body.wp-admin {
			background-color: '. $bg_color .'!important;
		}
		body.wp-admin #adminmenu li.wp-has-submenu.wp-not-current-submenu,
		body.wp-admin #adminmenu li.wp-has-submenu.wp-not-current-submenu > a {
			background-color: #FFF!important;
		}
		body.wp-admin #adminmenu li.wp-has-submenu.wp-not-current-submenu:hover {
			background-color: '. $secondary .'!important;
		}
		#adminmenu li.wp-has-submenu.wp-not-current-submenu .wp-submenu a {
			color: #262626!important;
		}
		#adminmenu li.wp-has-submenu.wp-not-current-submenu .wp-submenu li a:hover {
			color: '. $primary .'!important;
		}
		#collapse-button {
			font-size: 14px;
			margin-top: 10px;
		}
		#collapse-button:hover {
			color: '. $primary .';
			opacity: 0.7;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head,
		#adminmenu .wp-not-current-submenu .wp-submenu .wp-submenu-head {
			color: #262626;
		}
		#adminmenu .wp-has-submenu.wp-not-current-submenu.menu-top .wp-menu-name {
			background-color: '. $bg_color .'!important;
			background-color: #fff!important;
		}

		#adminmenu li.menu-top:hover,
	   	#adminmenu li.opensub>a.menu-top, 
	    #adminmenu li>a.menu-top:focus{
			background-color: #fff!important;
		}
		#adminmenu .current.menu-top a,
		#adminmenu .current.menu-top a .wp-menu-image:before {
			color: '. $primary .'!important;
		}
		#adminmenu .wp-has-submenu.wp-not-current-submenu.menu-top .wp-menu-name:hover,
		#adminmenu .wp-not-current-submenu.menu-top:hover {
			background-color: '. $secondary .'!important;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu {
			background-color: '. $secondary .';
		} 

		/* Core Buttons and Links */
		.wrap .add-new-h2, 
		.wrap .add-new-h2:active, 
		.wrap .page-title-action, 
		.wrap .page-title-action:active,
		.wrap .add-new-h2:hover, 
		.wrap .page-title-action:hover{
			color: '. $primary .';
			border-color: '. $primary .';
			background-color: #fff;
		}

		.wp-admin a, .wp-admin a:hover,
		.my-sites li a, .my-sites li a:hover,
		.wp-core-ui .button-link,
		.wp-core-ui .button-link:hover{
			color: '. $primary .';
		}

		.wp-core-ui .button,
		.wp-core-ui .button:hover,
		.wp-core-ui .button:focus{
			color: '. $primary .';
			border-color: '. $primary .';
			background-color: #fff;
		}

		.wp-core-ui .button-primary,
		.wp-core-ui .button-primary:hover,
		.wp-core-ui .button-primary:focus{
			color: #fff;
			background-color: '. $primary .';
			border-color: '. $primary .';
		}

		.view-switch a.current:before{
			color: '. $primary .';
		}

		.my-sites li a:hover{
			text-decoration: underline;
		}

		.page #main .entry-header {
			display: none!important;
		}
		.page.singular .entry-title {
			display: none;
		}
		.singular .entry-header {
			border: 0;
			padding: 0;
			margin: 0;
		}
		/*espacios*/
		html {
			margin-top: 50px !important;
		}
		html.wp-toolbar {
			padding-top: 0px;
		}
		/*admin bar*/
		#wpadminbar {
			height: 50px;
			background-color: #fff;
			box-shadow: 0 2px 4px rgb(0 0 0 / 8%);
			padding: 5px 21px 5px 82px;
			margin-bottom: 20px;
			box-sizing: border-box;	
		}
		#wpadminbar a:-webkit-any-link:focus-visible {
			outline-offset: 1px;
		}
		#wpadminbar a:focus-visible {
			outline: -webkit-focus-ring-color auto 1px;
		}

		/*top secondary*/
		#wp-admin-bar-top-secondary {
			display: inline-flex;
		}
		/* wp-logo*/
		#wpadminbar #wp-admin-bar-wp-logo > .ab-item {
			height: 39px;
			width: auto;
			display: flex;
			padding: 0;
			margin-right: 27px;
		}
		#wpadminbar #wp-admin-bar-wp-logo > .ab-item > img {
			width: auto;
			height: 39px;
		}
		#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
			content: none;
		}

		#wpadminbar .quicklinks .menupop ul li a{
			color: #262626!important;
		}
		#wpadminbar .ab-top-menu>li.hover>.ab-item,
		#wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus,
		#wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item,
		#wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus {
			background-color: '. $bg_color .';
			background-color: #fff;
		}
		#wpadminbar .ab-top-menu > li > .ab-item[aria-expanded="true"],
		#wpadminbar .ab-top-menu>li.hover>.ab-item[aria-expanded="true"],
		#wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item[aria-expanded="true"],
		#wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus[aria-expanded="true"]{
			background-color: '. $primary .'!important;
		}

		/*dropdown menu*/
		#wpadminbar .fas,
		#wpadminbar .far {
			font-family: "Font Awesome 5 Free"!important;
		}
		#wpadminbar .fa {
			font-family: "FontAwesome";
		}
		#wpadminbar .fab {
    		font-family: "Font Awesome 5 Brands"
		}
		#wpadminbar .ab-top-menu #wp-admin-bar-dropdown-menu .ab-sub-wrapper {
			width: 294px;
			max-height: 500px;
			padding: 10px;
			overflow-y: scroll;
			border: 1px solid rgba(0,0,0,0.15);
			border-radius: 5px;
			box-shadow: rgb(0 0 0 / 16%) 0 0 10px;
			top: 46px;
			box-sizing: border-box;
		}
		#wpadminbar .ab-top-menu #wp-admin-bar-my-account .ab-sub-wrapper{
			border: 1px solid rgba(0,0,0,0.15);
			border-radius: 5px;
			box-shadow: rgb(0 0 0 / 16%) 0 0 10px;
			top: 46px;
			right: -30px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu {
			display: flex;
			height: 39px;
			align-items: center;
			margin-right: 16px;
			order: 1;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu > .ab-item {
			background-image: url(' . plugin_dir_url(__FILE__) . 'apps-icon.svg);
			background-repeat: no-repeat;
			background-size: 24px 24px;
			background-position: center;
			cursor: pointer;
			border: none;
			padding: 4px;
			width: 36px;
			height: 36px;
			background-color: #F0F0F0;
			border-radius: 5px;
			box-sizing: border-box;
		}
		#wpadminbar #wp-admin-bar-menu-users:before{
			border: 10px solid transparent;
			border-bottom-color: #fff;
			bottom: 100%;
			content: " ";
			height: 0;
			width: 0;
			position: absolute;
			pointer-events: none;
			right: 38px;
			filter: drop-shadow(0 0 4px rgba(0,0,0,0.2));
		}
		#wpadminbar #wp-admin-bar-menu-users:after{
			background: #fff;
			top: 0;
			content: " ";
			height: 6px;
			width: 24px;
			position: absolute;
			pointer-events: none;
			right: 36px;
		}
		#wpadminbar #wp-admin-bar-menu-users ul a {
			padding: 10px 20px 10px;
			display: flex;
			align-items: center;
		}
		#wpadminbar #wp-admin-bar-menu-users ul a > i {
			font-size: 20px;
			line-height: 1em;
			width: 30px;
		}
		#wpadminbar #wp-admin-bar-menu-users ul a > span {
			font-size: 16px;
			line-height: 1em;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul {
			padding: 15px 2px 0 0;
			list-style: none;
			display: grid;
			flex-wrap: wrap;
			padding: 2px 2px 10px 0;
			margin-bottom: 0;
			grid-template-columns: 133px 133px;
			grid-gap: 12px 2px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul:not(:first-child){
			border-top: 1px solid #D9D9D9;
			padding-top: 20px;
    		margin-top: 8px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a {
			min-width: auto;
			height: auto;
			line-height: inherit;
			display: flex;
			flex-direction: row;
			align-items: center;
			justify-content: start;
			text-align: center;
			text-decoration: none;
			background-color: #F0F0F0;
			color: #262626;
			margin-right: 8px;
			border-radius: 5px;
			padding: 5px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .icon {
			width: 40px;
			height: 40px;
			background-color: transparent;
			border-radius: 0.25rem;
			margin-right: 0;
			display: flex;
			align-items: center;
			justify-content: center;
			color: ' . $primary .';
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .icon:hover {
			filter: brightness(115%);
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .icon i {
			font-size: 19px;
			line-height: 40px;
			color: inherit;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps hr {
			width: 100%;
			margin: 0 auto 20px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .text {
			text-align: left;
			margin-left: 0;
			font-size: 13px;
			font-weight: lighter;
			line-height: 15px;
			overflow-wrap: anywhere;
			position: relative;
			top: 1px;
			max-width: 71px;
		}

		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a:hover {
			background-color: ' . $primary .';
			color: #fff!important;
		}

		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a:hover .icon{
			color: #fff;
		}
		
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a.ab-item {
			white-space: break-spaces;
		}

		#wpadminbar li.hover>.ab-sub-wrapper{
			display: none;
		}

		#wpadminbar.nojs li:hover>.ab-sub-wrapper {
			display: block;
		}

		#wpadminbar li > .ab-sub-wrapper.hide {
			display: none;
		}

		#wpadminbar li > .ab-sub-wrapper.show {
			display: block;
		}

		/*notificaciones*/
		#wpadminbar #wp-admin-bar-notifications {
			display: flex;
			height: 66px;
			align-items: center;
			margin-right: 40px;
			order: 2;
		}
		#wpadminbar #wp-admin-bar-notifications > .ab-item {
			background-image: url(' . plugin_dir_url(__FILE__) . 'bell.svg);
			background-repeat: no-repeat;
			background-size: contain;
			width: 32px;
			height: 32px;
			padding: 0;
			cursor: pointer;
		}
		/*digitaldemocratic*/
		#wpadminbar #wp-admin-bar-digital-democratic {
			display: flex;
			height: 39px;
			width: 39px;
			align-items: center;
			margin-right: 0;
			order: 4;
		}
		#wpadminbar #wp-admin-bar-digital-democratic > .ab-item {
			background-image: url('.$product_logo.');
			background-repeat: no-repeat;
			background-size: contain;
			width: 30px;
			height: 28px;
			padding: 0;
			cursor: pointer;
			position: relative;
			top: 1px;
		}
		/*counter*/
		#wpadminbar #wp-admin-bar-notifications .counter {
			position: absolute;
			top: 10px;
			right: -13px;
			background-color: red;
			width: 20px;
			height: 22px;
			border-radius: 6px;
			color: white;
			font-size: 10px;
			text-align: center;
			line-height: 22px;
		}
		/*mis sitios*/
		#wpadminbar #wp-admin-bar-my-sites,
		#wpadminbar #wp-admin-bar-mis-sitios {
			display: flex;
			height: 39px;
			align-items: center;
		}
		#wpadminbar #wp-admin-bar-my-sites a,
		#wpadminbar #wp-admin-bar-my-sites a:before,
		#wpadminbar #wp-admin-bar-mis-sitios a,
		#wpadminbar #wp-admin-bar-mis-sitios a:before {
			color: #262626;
		}
		#wpadminbar #wp-admin-bar-my-sites .ab-sub-wrapper a:hover,
		#wpadminbar #wp-admin-bar-my-sites .ab-sub-wrapper a:hover:before,
		#wpadminbar #wp-admin-bar-mis-sitios .ab-sub-wrapper a:hover,
		#wpadminbar #wp-admin-bar-mis-sitios .ab-sub-wrapper a:hover:before {
			color: '. $primary .';
			background-color: '. $secondary.';
		}

		#wpadminbar #wp-admin-bar-my-sites a.ab-item:before,
		#wpadminbar #wp-admin-bar-mis-sitios a.ab-item:before {
			top: 0;
			font-size: 32px;
			line-height: 66px;
			padding: 0;
		}
		#wpadminbar #wp-admin-bar-mis-sitios:before {
			/*content: url(' . plugin_dir_url(__FILE__) . 'menu_apps.svg);*/
			content: "\f116";
			font-family: dashicons;
			font-size: 18px;
			color: #000;
			line-height: 1em;
		}
		/*#wpadminbar #wp-admin-bar-mis-sitios a.ab-item:before {
			line-height: 57px;
		}*/
		#wpadminbar #wp-admin-bar-my-sites a.ab-item,
		#wpadminbar #wp-admin-bar-mis-sitios a.ab-item {
			height: 39px;
			line-height: 38px;
			font-size: 15px;
		}
		#wpadminbar .menupop .menupop>.ab-item .wp-admin-bar-arrow:before {
			line-height: 50px;
			padding: 0;
			top: 0;
		}
		#wpadminbar .ab-sub-wrapper li.menupop {
			height: 50px;
			line-height: 50px;
			background-color: '. $bg_color .';
			background-color: #FFF;
		}
		#wpadminbar .quicklinks .menupop ul.ab-sub-secondary,
		#wpadminbar .quicklinks .menupop ul.ab-sub-secondary .ab-submenu {
			background-color: '. $bg_color .'!important;
			background-color: #FFF!important;
		}
		#wpadminbar .ab-sub-wrapper li.menupop a.ab-item {
			height: 50px!important;
			line-height: 50px!important;
		}
		#wpadminbar .ab-sub-wrapper li.menupop a.ab-item:hover {
			color: '. $primary .'!important;

		}
		#wpadminbar .ab-sub-wrapper .ab-submenu {
			padding: 0;
		}
		#wpadminbar .ab-top-menu .menupop .ab-sub-wrapper .ab-sub-wrapper {
			top: 30px!important;
		}
		#wpadminbar .quicklinks li .blavatar:before {
			color: #262626!important;
		}
		#wpadminbar .quicklinks li a:hover .blavatar:before {
			color: '. $primary .'!important;
			background-color: '. $secondary.';
		}
		/*usuario*/
		#wpadminbar #wp-admin-bar-my-account {
			display: flex;
			height: 39px;
			align-items: center;
			margin-right: 16px;
			order: 3;
		}
		#wpadminbar #wp-admin-bar-my-account img {
			cursor: pointer;
		}
		#wpadminbar .ab-top-menu .menupop .ab-sub-wrapper,
		#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper {
			top: 39px;
			background: '. $bg_color. ';
			background: #FFF;
		}
		/*#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper a {
			color: #262626!important;
		}*/
		#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper a:hover {
			background-color: '. $secondary.';
		}
		#wpadminbar .ab-top-secondary #wp-admin-bar-dropdown-menu .ab-sub-wrapper a:hover {
			background-color: transparent;
		}
		#wpadminbar #wp-admin-bar-my-account > .ab-item {
			padding: 0;
			height: 39px;
			width: 39px;
			border: 2px solid #F0F0F0;
			border-radius: 50%;
			box-sizing: border-box;
			overflow: hidden;
		}
		#wpadminbar #wp-admin-bar-my-account .ab-sub-wrapper a:hover{
			background-color: whitesmoke;
		}
		#wpadminbar #wp-admin-bar-my-account.with-avatar > .ab-item img {
			border: 0;
			height: 100%;
			width: 100%;
			border-radius: 50%;
			margin: 0;
			object-fit: cover;
		}

		#wpadminbar ::-webkit-scrollbar{
			width: 12px;
			height: 12px;
		}

		#wpadminbar ::-webkit-scrollbar-thumb{
			background: #dbdbdb;
			border-radius: 10px;
			border: 2px solid transparent;
			background-clip: content-box;
		}

		#wpadminbar ::-webkit-scrollbar-track-piece {
			background-color: transparent;
		}

		@media screen and (max-width: 782px) {
			#adminmenu { top: 0!important; }
			#wpbody { padding-top: 0!important; }
			html #wpadminbar {
				height: 50px;
				position: fixed;
			}
			.wp-responsive-open #wpadminbar #wp-admin-bar-menu-toggle .ab-item {
				background-color: '. $bg_color .';
				background-color: #FFF;
			}
			.wp-responsive-open #wpadminbar #wp-admin-bar-menu-toggle .ab-icon:before {
				color: '. $primary .';
			}
			#wpadminbar #wp-admin-bar-menu-toggle .ab-icon:before {
				line-height: 49px;
				color: #000;
				font-size: 22px;
				line-height: 39px;
				width: 22px;
				height: 22px;
			}
			#wpadminbar #wp-admin-bar-menu-toggle a {
				height: 39px;
				border: 0!important;
			}
			#wpadminbar .quicklinks li#wp-admin-bar-my-account.with-avatar>a img {
				position: relative;
				height: 39px;
				width: 39px;
				top: 0;
				right: 0;
			}
			#wpadminbar .quicklinks li#wp-admin-bar-my-account.with-avatar>a {
				height: 39px;
			}
			#wpadminbar #wp-admin-bar-my-account {
				margin-right: 20px;
				position: relative;
			}
			#wpadminbar #wp-admin-bar-user-info .display-name {
				color: #000;
			}
			#wpadminbar .ab-top-menu>.menupop>.ab-sub-wrapper {
				left: auto;
			}
			#wpadminbar .ab-top-menu>.menupop>.ab-sub-wrapper .ab-item {
				padding: 0 16px;
			}
			#wpadminbar .ab-top-menu .menupop .ab-sub-wrapper .ab-sub-wrapper {
				top: 50px!important;
			}
			#wpadminbar #wp-admin-bar-mis-sitios{
				display: none;
			}
			#wpadminbar #wp-admin-bar-my-account > .ab-item {
				text-indent: unset;
				width: 39px;
			}
		}
		/*fullscreenMode active always*/
		body:not(.is-fullscreen-mode) .interface-interface-skeleton {
			left: 0!important;
			top: 66px!important;
		}
		/*body:not(.is-fullscreen-mode) .interface-interface-skeleton .interface-interface-skeleton__header .edit-post-header:before {
			content: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\' width=\'21\' height=\'21\' viewBox=\'0 0 24 24\'><path d=\'M13.427 3.021h-7.427v-3.021l-6 5.39 6 5.61v-3h7.427c3.071 0 5.561 2.356 5.561 5.427 0 3.071-2.489 5.573-5.561 5.573h-7.427v5h7.427c5.84 0 10.573-4.734 10.573-10.573s-4.733-10.406-10.573-10.406z\' fill=\'#FFF\'/></svg>")!important;
			margin: 0;
			padding: 0;
			display: block;
			border: none;
			background: #23282e;
			border-radius: 0;
			height: 61px;
			width: 60px;
			margin-bottom: -1px;
			line-height: 73px;
			text-align: center;
			cursor: pointer;
		}*/
		/*icon wordpress en edit post header*/
		a.components-button.edit-post-fullscreen-mode-close.has-icon {
			background-image: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\' width=\'24\' height=\'24\' viewBox=\'0 0 24 24\'><path d=\'M13.427 3.021h-7.427v-3.021l-6 5.39 6 5.61v-3h7.427c3.071 0 5.561 2.356 5.561 5.427 0 3.071-2.489 5.573-5.561 5.573h-7.427v5h7.427c5.84 0 10.573-4.734 10.573-10.573s-4.733-10.406-10.573-10.406z\' fill=\'#FFF\'/></svg>")!important;
			background-position: center;
			margin: 0;
			padding: 0;
			background-size: 35% 70%;
			background-repeat: no-repeat;
			color: rgba(0,0,0,0);
		}
		a.components-button.edit-post-fullscreen-mode-close.has-icon svg { display: none; }

		@media screen and (max-width: 600px){
			#wpadminbar{
				padding-left: 21px;
			}
			#wpadminbar li#wp-admin-bar-wp-logo {
				display: block;
			}

			#wpadminbar .ab-top-menu #wp-admin-bar-dropdown-menu .ab-sub-wrapper {
				width: 264px;
				right: -70px;
			}
		}

		iframe{
			width: 100%;
		}

		@media screen and (min-width: 1200px) {
			#adminmenu, #adminmenu .wp-submenu, #adminmenuback, #adminmenuwrap{
				width: 220px;
			}
			#wpcontent, #wpfooter {
				margin-left: 220px;
			}
			#adminmenu .wp-submenu{
				left: 220px;
			}
		}

	</style>';

if ( !current_user_can( 'mange_networks' ) ) {
	echo '<style>
			#adminmenumain {
					display: none!important;
				}
				body.wp-admin #wpfooter, body.wp-admin #wpcontent {
					margin-left: 0!important;
					padding: 20px 40px;
				}
				#wpadminbar #wp-admin-bar-notifications {
					display: none!important;
				}
			</style>';
}

/*echo '<script>
	jQuery("body:not(.is-fullscreen-mode) .interface-interface-skeleton .interface-interface-skeleton__header .edit-post-header").preppendHTML("<a href=\'{$referrer}\'></a>");
	</script>';*/
