(function($) {
  $(document).ready(function() {

    $dropdowns = $('#wp-admin-bar-top-secondary .dropdown-menu > .ab-item');

    if($dropdowns.length){

      $dropdowns.on('click', function(e){
        e.preventDefault();
      });

      var menu = new DisclosureMenu({
        menuElement: document.querySelector("#wp-admin-bar-top-secondary"),
        submenuItemSelector: "li.dropdown-menu",
        submenuSelector: 'div.ab-sub-wrapper:last-child'
      });

    }

  });
})(jQuery);




