( function ( $, dd ) {

    'use strict';

    wp.domReady(function () {

        var blocks = wp.blocks.getBlockTypes();
        var root_blocks = [];
        var enabled_blocks = [];

        // Get blocks with no parent
        blocks.forEach(function ( block ) {
            if( ! block.parent ){
                root_blocks.push(block.name);
            }
        });

        if ( dd.blocks ){

            $.each( dd.blocks, function ( key, enabled ) {
                if ( key === 'embeds'){ // embeds category (unregister disabled variants)
                    if ( $.isArray( enabled ) ) {
                        var variants = wp.blocks.getBlockVariations( 'core/embed' );
                        $.each( variants, function ( key, variant ) {
                            if ( $.inArray( variant.name , enabled ) === -1 ){
                                wp.blocks.unregisterBlockVariation( 'core/embed', variant.name );
                            }
                        });
                    }  
                }
                /*
                else { // another categories
                    $.each( enabled, function ( key, block ) {
                        enabled_blocks.push(block);
                    });
                }*/
            });

            // Unregister disabled blocks 
            // (NOTE: not unregistered becose affects existing blocks) 
            /*$.each( root_blocks, function ( key, block ) {
                if ( $.inArray( block , enabled_blocks ) === -1 ){
                    wp.blocks.unregisterBlockType( block );
                }
            });*/

        }
    
    });

} )( jQuery, typeof dd_blocks_settings !== 'undefined' ? dd_blocks_settings : {} );

