<?php
/*
Plugin Name: Wemcor Functions
Plugin URI:
Description: Funciones adicionales para los plugins de wemcor
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if (!defined('ABSPATH')) exit; // Exit if accessed directly


/**
 * Demo mode for local development outside DD
 * DISABLE in DD environment
 */
defined('DD_DEMO_MODE') or define('DD_DEMO_MODE', false);

/*
 * Load Text Domain
 * Load the plugin textdomain.
 */
add_action('muplugins_loaded', 'wemcor_load_text_domain');
function wemcor_load_text_domain() {
	$plugin_url = plugin_dir_url(__FILE__);
	load_muplugin_textdomain('wemcor-multisite', 'languages');
}

/*
 * Enqueues
 */
add_action('admin_print_styles', 'wemcor_enqueue_styles');
function wemcor_enqueue_styles() {
	$plugin_url = plugin_dir_url(__FILE__);
	wp_enqueue_style('dd-admin-styles', $plugin_url . 'assets/css/admin-style.css');
}

add_action('admin_enqueue_scripts', 'wemcor_enqueue_scripts');
function wemcor_enqueue_scripts() {
	$plugin_url = plugin_dir_url(__FILE__);
	wp_register_script('dd-admin-scripts', $plugin_url . 'assets/js/scripts.js', array('jquery'), '2.0', true);
	wp_enqueue_script('dd-admin-scripts');
}

add_action('wp_enqueue_scripts', 'wemcor_enqueue_frontend', 999);
function wemcor_enqueue_frontend() {
	$blog_id = get_current_blog_id();
	$activate_theme = function_exists('get_blog_option') ? get_blog_option($blog_id, 'stylesheet') : get_option('stylesheet');
	wp_enqueue_style('dd-frontend-styles', network_site_url('/wp-content/mu-plugins/assets/css/' . $activate_theme . '.css'));
}

// cargar título predeterminado cuando se hace click en nueva página
add_filter('default_title', 'wemcor_default_title_on_page', 10, 2);
function wemcor_default_title_on_page($title, $post) {
	if ('page' === $post->post_type) $title = __('New page', 'wemcor-multisite');
	return $title;
}

// cargar contenido predeterminado cuando se hace click en añadir nueva página
add_filter('default_content', 'wemcor_default_content_on_page', 10, 2);
function wemcor_default_content_on_page($content, $post) {
	if ('page' === $post->post_type) {
		$content = '<!-- wp:generateblocks/container {"uniqueId":"a4c6887b","containerWidth":960,"paddingTop":"180","paddingRight":"100","paddingBottom":"180","paddingLeft":"100","paddingTopTablet":"120","paddingBottomTablet":"120","paddingTopMobile":"60","paddingRightMobile":"20","paddingBottomMobile":"60","paddingLeftMobile":"20","backgroundColor":"#226e93","gradient":true,"gradientDirection":0,"gradientColorOne":"#242424","gradientColorOneOpacity":0.4,"gradientColorStopOne":0,"gradientColorTwo":"#242424","gradientColorTwoOpacity":0.4,"gradientColorStopTwo":100,"bgImage":{"id":143,"image":{"url":"%%DOMAIN%%/encabezamiento-1.png","height":544,"width":725,"orientation":"landscape"}},"bgOptions":{"selector":"pseudo-element","opacity":1,"overlay":false,"position":"center center","size":"cover","repeat":"no-repeat","attachment":""},"showAdvancedTypography":true,"align":"full","isDynamic":true} -->
		<!-- wp:heading {"textAlign":"center","level":1,"style":{"typography":{"fontSize":80}},"textColor":"white"} -->
		<h1 class="has-text-align-center has-white-color has-text-color" style="font-size:80px">Títol de la plana web</h1>
		<!-- /wp:heading -->
		<!-- /wp:generateblocks/container -->

		<!-- wp:paragraph -->
		<p style="margin-top: 30px;">Afegeix continguts de la plana a partir d\'aqui</p>
		<!-- /wp:paragraph -->
		
		<!-- wp:spacer {"height":"2rem"} -->
		<div style="height:2rem" aria-hidden="true" class="wp-block-spacer"></div>
		<!-- /wp:spacer -->
		';

		$content = str_replace('%%DOMAIN%%', 'https://' . $_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"] . '/wp-content/mu-plugins/images', $content);
	}


	return $content;
}

//prevenir Cannot modify header information - headers already
add_action('wp_loaded', 'wemcor_prebvent_headers_custom_redirect');
function wemcor_prebvent_headers_custom_redirect() {
	if (is_admin() && isset($_REQUEST['action']) && ('add-site' === $_REQUEST['action'] || 'import-site' === $_REQUEST['action'])) ob_start();
}

//remove notices in admin panel
add_action('in_admin_header', 'wemcor_hide_notices_to_all', 99);
function wemcor_hide_notices_to_all() {
	//if (!is_super_admin()) {
	remove_all_actions('user_admin_notices');
	remove_all_actions('admin_notices');
	//}
}

//remove nav-menus from customize WordPress
/*add_action('customize_register', function ($WP_Customize_Manager) {
	if (isset($WP_Customize_Manager->nav_menus) && is_object($WP_Customize_Manager->nav_menus)) {
		remove_filter('customize_refresh_nonces', array($WP_Customize_Manager->nav_menus, 'filter_nonces'));
		remove_action('wp_ajax_load-available-menu-items-customizer', array($WP_Customize_Manager->nav_menus, 'ajax_load_available_items'));
		remove_action('wp_ajax_search-available-menu-items-customizer', array($WP_Customize_Manager->nav_menus, 'ajax_search_available_items'));
		remove_action('customize_controls_enqueue_scripts', array($WP_Customize_Manager->nav_menus, 'enqueue_scripts'));
		remove_action('customize_register', array($WP_Customize_Manager->nav_menus, 'customize_register'), 11);
		remove_filter('customize_dynamic_setting_args', array($WP_Customize_Manager->nav_menus, 'filter_dynamic_setting_args'), 10, 2);
		remove_filter('customize_dynamic_setting_class', array($WP_Customize_Manager->nav_menus, 'filter_dynamic_setting_class'), 10, 3);
		remove_action('customize_controls_print_footer_scripts', array($WP_Customize_Manager->nav_menus, 'print_templates'));
		remove_action('customize_controls_print_footer_scripts', array($WP_Customize_Manager->nav_menus, 'available_items_template'));
		remove_action('customize_preview_init', array($WP_Customize_Manager->nav_menus, 'customize_preview_init'));
		remove_filter('customize_dynamic_partial_args', array($WP_Customize_Manager->nav_menus, 'customize_dynamic_partial_args'), 10, 2);
	}
}, -1);*/

// remove menu items from customize WordPress
add_action('customize_register', 'remove_customizer_settings', 99);
function remove_customizer_settings($wp_customize) {
	$blog_id = get_current_blog_id();
	$stylesheet = function_exists('get_blog_option') ? get_blog_option($blog_id, 'stylesheet') : get_option('stylesheet');
	/*
	 * Poner condicional para roles y capabilities
	 *
	 */
	//$wp_customize->remove_section( 'title_tagline');
	if ('twentyten' === $stylesheet)  $wp_customize->remove_section('colors');
	//$wp_customize->remove_section( 'header_image');
	//$wp_customize->remove_section( 'background_image');
	//$wp_customize->remove_section( 'static_front_page');
	$wp_customize->remove_section('custom_css');

	if ('generatepress' === $stylesheet) {
		$wp_customize->remove_section('generatepress_upsell_section');
	}
}

// add meta tags in header according theme
add_action('wp_head', 'wemcor_add_meta_tags');
function wemcor_add_meta_tags() {
	$blog_id = get_current_blog_id();
	$activate_theme = function_exists('get_blog_option') ? get_blog_option($blog_id, 'stylesheet') : get_option('stylesheet');
	if ('twentyten' === $activate_theme) {
?>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<?php }
}

function wemcor_fullscreen_editor() {
	$js_code = "jQuery(document).ready(function(){" .
		"   var isFullScreenMode = wp.data.select('core/edit-post').isFeatureActive('fullscreenMode');" .
		"   if ( !isFullScreenMode )" .
		"       wp.data.dispatch('core/edit-post').toggleFeature('fullscreenMode');" .
		"});";
	wp_add_inline_script('wp-blocks', $js_code);
}
//add_action( 'enqueue_block_editor_assets', 'wemcor_fullscreen_editor' );

// Allow SVG
add_filter('wp_check_filetype_and_ext', function ($data, $file, $filename, $mimes) {

	global $wp_version;
	if ($wp_version !== '4.7.1') {
		return $data;
	}

	$filetype = wp_check_filetype($filename, $mimes);

	return [
		'ext'             => $filetype['ext'],
		'type'            => $filetype['type'],
		'proper_filename' => $data['proper_filename']
	];
}, 10, 4);



// Eliminar el selector de idioma de la página de login
add_filter('login_display_language_dropdown', '__return_false');

/**
 * Disable site admin email verification
 */
add_filter( 'admin_email_check_interval', '__return_false' );



// remove screen options
function wemcor_remove_screen_options() {
	return false;
}
add_filter('screen_options_show_screen', 'wemcor_remove_screen_options');


// remove tab help in admin panel (deprecated filter)
/*add_filter('contextual_help_list', 'contextual_help_list_remove');
function contextual_help_list_remove() {
	global $current_screen;
	$current_screen->remove_help_tabs();
}*/

// remove tab help in admin panel (new)
add_action( 'admin_head', 'wemcor_remove_contextual_help' );
function wemcor_remove_contextual_help() {
    $screen = get_current_screen();
    $screen->remove_help_tabs();
}



/**
 * Allow basic svg for GenerateBlocks
 * https://docs.generateblocks.com/article/fix-svg-icons-not-saving/
 */
add_filter( 'wp_kses_allowed_html', 'wemcor_allow_basic_svg', 10, 2);
function wemcor_allow_basic_svg( $tags, $context ) {
	if ( 'post' === $context ) {
		$tags['svg'] = array(
			'viewbox' => true,
			'filter' => true,
			'xmlns' => true,
			'class' => true,
			'preserveaspectratio' => true,
			'aria-hidden' => true,
			'data-*' => true,
			'role' => true,
			'height' => true,
			'width' => true,
		);

		$tags['path'] = array(
			'class' => true,
			'fill' => true,
			'd' => true,
		);

		$tags['filter'] = array(
			'id' => true,
		);
	}

	return $tags;
}


/**
 *  Get super admin user id
 */
function wemcor_get_super_admin() {
	$super_admin_ids = get_super_admins();
	$super_admin = get_user_by('login', $super_admin_ids[0]);
	$super_admin_id = $super_admin->ID;

	return $super_admin_id;
}

/**
 * Assign site to users
 */
function wemcor_asign_site_to_users( $id, $user_id ) {

	if (! function_exists('add_user_to_blog') ){
		return false;
	}

	//get users by role manager / administrator
	$args = array(
	    'role__in'     => array('manager', 'administrator'),
	    'role__not_in' => array('teacher', 'student'),
	    'blog_id'      => $id
	);
	$users = get_users( $args );
	foreach( $users as $user ) {
		add_user_to_blog( $id, $user_id, 'manager' );
	}

}

/**
 * Change user role ???
 */
function wemcor_change_role_user($id, $user_id) {

	if (! function_exists('add_user_to_blog') ){
		return false;
	}
	
	$user = new WP_User( $user_id );
	// Roles
	$roles = $user->roles;
	// Remove role. Creado blog nuevo con solo un usuario que es administrador
	// remove_user_from_blog($user_id , $id);
	// Add role. asignamos el mismo usuario al mismo blog pero con role student o teacher (según el que sea cuando ha creado el blog)

	if( in_array('manager', $roles) ) add_user_to_blog( $id, $user_id, 'manager' );
	else if( in_array('teacher', $roles) ) add_user_to_blog( $id, $user_id, 'teacher' );
	else if( in_array('student', $roles) ) add_user_to_blog( $id, $user_id, 'student' );

}

/**
 * Set blog options
 */
function wemcor_set_site_options($id, $options) {
	foreach( $options as $option => $value ) {
		update_blog_option($id, $option, $value);
	}
}


/**
 * Delete all pages from new site
 */
function wemcor_delete_all_pages_new_site() {
		$page_ids = get_all_page_ids();
		foreach( $page_ids as $page_id ) {
			wp_delete_post( $page_id, true );
		}
}


/**
 * Assign user site role ??=
 */
function wemcor_get_user_site_role($id, $user_id) {
	$user_meta = get_userdata($user_id);
	$user_roles = $user_meta->roles;
	if ( in_array( 'manager', $user_roles ) ) return 'manager';
	if ( in_array( 'teacher', $user_roles ) ) return 'teacher';
	if ( in_array( 'student', $user_roles ) ) return 'student';

	return 'subscriber';
}


/**
 *  Get internal api urls (added by Raúl Solana to enable Demo Mode)
 */
function wemcor_get_api_internal_url($path) {
	if (DD_DEMO_MODE) {
		return plugins_url('dd-demo/api/api-internal-' . str_replace('/', '-', $path) . '.json', __FILE__);
	}
	// return 'http://isard-sso-admin:9000/api/internal/' . $path;
	return 'http://dd-sso-admin:9000/api/internal/' . $path;
}
