<?php
/*
Plugin Name: Digital Democratic CLI
Description: Digital Democratic WP Cli Commands
Version:     1.0
Author:      tresipunt
Author URI:  https://tresipunt.com
Text Domain: wemcor-multisite
Domain Path:  /languages
*/


if (defined('WP_CLI') && WP_CLI) {

    class DD_CLI {


        /**
         * The current version of the plugin.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string    $version
         */
        protected $version = '1.0.0';

        /**
         * Private properties
         */
        private $GS2WP_PATH         =  "/var/www/gsite2wordpress/gsite_scraping/";      // gsite2wordpress python script path
        private $ST_OPTION          = 'dd_import_status';                               // site status option
        private $URL_OPTION         = 'dd_import_url';                                  // site import url
        private $TS_OPTION          = 'dd_import_time';                                 // site import timestamp
        private $STATUS_PENDING     = 'pending';                                        // Import pending status
        private $STATUS_PROCESSING  = 'processing';                                     // Import processing status
        private $STATUS_SUCCESS     = 'success';                                        // Import success status
        private $STATUS_ERROR       = 'error';                                          // Import error status
        private $IMPORT_DIR_NAME    = 'dd_gsite';                                       // Import directory name in site uploads


        /**
         * Import sites wp cli command
         * usage: wp dd import_sites
         */
        public function import_sites($args, $assoc_args) {

            $sites = get_sites(array(
                'number' => 0, // unlimited
            ));

            $amount = count($sites);
            $success = 0;
            $error = 0;

            $progress = \WP_CLI\Utils\make_progress_bar('Processing sites', $amount);

            foreach ($sites as $site) {

                $dd_import_status = get_blog_option($site->blog_id, $this->ST_OPTION);
                $uploads = wp_upload_dir();

                // Continue if no import status pending or processsing
                if (!$dd_import_status || ($dd_import_status !== $this->STATUS_PENDING && $dd_import_status !== $this->STATUS_PROCESSING)) {
                    $progress->tick();
                    continue;
                }

                $dd_import_url = get_blog_option($site->blog_id, $this->URL_OPTION);

                // Continue if no import status url
                if (!$dd_import_url) {
                    update_blog_option($site->blog_id, $this->ST_OPTION, $this->STATUS_ERROR);
                    $error++;
                    $progress->tick();
                    continue;
                }

                switch_to_blog($site->blog_id);

                $local_time  = current_datetime();
                $local_timestamp = $local_time->getTimestamp() + $local_time->getOffset();

                // Check if the site has been in processing too long and reset to pending
                if ($dd_import_status === $this->STATUS_PROCESSING) {

                    $dd_import_ts = intval(get_blog_option($site->blog_id, $this->TS_OPTION));
                    $days_diff = ($local_timestamp - $dd_import_ts) / 60 / 60 / 24;

                    if ($days_diff > 1) {

                        // delete all pages
                        $page_ids = get_all_page_ids();
                        foreach( $page_ids as $page_id ) {
                            wp_delete_post( $page_id, true );
                        }
                        // update import options
                        update_blog_option($site->blog_id, $this->TS_OPTION, $local_timestamp);
                        update_blog_option($site->blog_id, $this->ST_OPTION, $this->STATUS_PENDING);

                    }

                    restore_current_blog();

                    $progress->tick();
                    continue;
                }

                // Start processing pending site:

                update_blog_option($site->blog_id, $this->TS_OPTION, $local_timestamp);
                update_blog_option($site->blog_id, $this->ST_OPTION, $this->STATUS_PROCESSING);

                
                $uploads = wp_upload_dir();
                $cmd = '(cd ' . $this->GS2WP_PATH . ' && python3 gsite2wordpress.py -o ' . $uploads['basedir'] . '/' . $this->IMPORT_DIR_NAME . ' ' . $dd_import_url . ')';

                ob_start();
                $cmd_result = system($cmd . ' 2>&1');
                ob_get_clean();

                /*
                \WP_CLI::line($cmd_result);
                if(!$cmd_result){
                    \WP_CLI::line('CMD Error');
                }else{
                    \WP_CLI::line('CMD Success');
                }
                */
                
        
                $process_result = $this->process_site($site->blog_id);
                if ($process_result) {
                    $success++;
                    update_blog_option($site->blog_id, $this->ST_OPTION, $this->STATUS_SUCCESS);
                } else {
                    $error++;
                    update_blog_option($site->blog_id, $this->ST_OPTION, $this->STATUS_ERROR);
                }

                restore_current_blog();

                $progress->tick();
            }

            $progress->finish();

            \WP_CLI::success($success . ' sites imported!!!');

            if ($error > 0) {
                \WP_CLI::error($error . ' sites not imported!!!');
            }
        }



        /**
         * Process site import images and pages
         * 
         * @param int $site_id wp blog_id
         * 
         * @return bool process result
         */
        private function process_site($site_id) {

            $uploads = wp_upload_dir();
            $import_path = $uploads['basedir'] . DIRECTORY_SEPARATOR . $this->IMPORT_DIR_NAME;
            $pages_path = $import_path . DIRECTORY_SEPARATOR . 'pages';
            $images_path = $import_path . DIRECTORY_SEPARATOR . 'images';
            $pages_prefix = '[pages]';
            $images_prefix = '[images]';
            $common = $import_path . DIRECTORY_SEPARATOR . 'common.json';
            $pages = $pages_data = $images = $images_data = array();

            require_once(ABSPATH . 'wp-admin/includes/image.php');

            $images = $this->scan_files($images_path);

            if (!empty($images)) {

                $iindex = 1; // index suffix for new image name

                foreach ($images as $image) {

                    try {

                        $iparts = pathinfo($image);
                        $iinfo = getimagesize($image);
                        $isize = filesize($image);

                        if (!$iinfo || !in_array($iinfo['mime'], get_allowed_mime_types())) {
                            throw new Exception('WordPress doesn\'t allow this type of uploads.');
                        }

                        if ($isize > wp_max_upload_size()) {
                            throw new Exception('Image is too large than expected.');
                        }

                        $images_data[$image] = array(
                            'success' => true,
                            'search' => isset($iparts['extension']) ?  $iparts['filename'] . '.' . $iparts['extension'] : $iparts['filename'],
                            'mime' => $iinfo['mime'],
                            'extension' => isset($iparts['extension']) ? '.' . $iparts['extension'] : '.' . $this->get_img_extension($iinfo['mime'])
                        );
                    } catch (Exception $e) {

                        \WP_CLI::line($e->getMessage());

                        $images_data[$image] = array(
                            'success' => false,
                            'search' => isset($iparts['extension']) ?  $iparts['filename'] . '.' . $iparts['extension'] : $iparts['filename']
                        );
                    }
                }

                foreach ($images_data as $key => $data) {

                    $new_file_name = 'image_' . $site_id . '_' . $iindex;
                    $new_file_path = $uploads['path'] . DIRECTORY_SEPARATOR . $new_file_name . $data['extension'];

                    while (file_exists($new_file_path)) {
                        $iindex++;
                        $new_file_name = 'image_' . $site_id . '_' . $iindex;
                        $new_file_path = $uploads['path'] . DIRECTORY_SEPARATOR . $new_file_name . $data['extension'];
                    }

                    $new_file_url = $uploads['url'] . '/' . basename($new_file_path);

                    if ($data['success'] && copy($key, $new_file_path)) {

                        $upload_id = wp_insert_attachment(array(
                            'guid'           => $new_file_url,
                            'post_mime_type' => $data['mime'],
                            'post_title'     => $new_file_name,
                            'post_content'   => '',
                            'post_status'    => 'inherit'
                        ), $new_file_path);

                        wp_update_attachment_metadata($upload_id, wp_generate_attachment_metadata($upload_id, $new_file_path));

                        $images_data[$key]['replace'] = $new_file_url;
                    } else {
                        $images_data[$key]['replace'] = '';
                    }
                }
            }

            // If common.json process the pages defined there
            if (file_exists($common)) {

                $json = file_get_contents($common);
                $json_data = json_decode($json, true);

                if ($json_data & !empty($json_data)) {

                    $menu = $json_data[0]['menu'];
                    $pages_data = $this->get_menu_pages($menu, $pages_path);
                }
            }else{
                WP_CLI::line('Config file common.json not found');
            }

            // If no pages in common.json scan pages dir
            if (empty($pages_data)) {
                $pages = $this->scan_files($pages_path, '/\.html$/');
                foreach ($pages as $p) {
                    $parts = pathinfo($p);
                    //\WP_CLI::line($parts);
                    $pages_data[$parts['filename']] = array(
                        'title' => ucfirst($parts['filename']),
                        'path' => $p,
                        'parent' => 0
                    );
                }
            }

            // No pages: site error
            if (empty($pages_data)) {
                $this->rrmdir($import_path);
                WP_CLI::line('No pages found');
                return false;
            }

            $page_on_front = false;


            foreach ($pages_data as $k => $p) {

                //\WP_CLI::line(print_r($p));
                //\WP_CLI::line(print_r($content));

                $postarr = [
                    'post_title' => $p['title'],
                    'post_content' => '',
                    'post_status' => 'publish',
                    'post_type' => 'page',
                    'post_parent' => (isset($p['parent']) && isset($pages_data[$p['parent']]['id'])) ? $pages_data[$p['parent']]['id'] : 0
                ];

                $page_id = wp_insert_post($postarr, false, false);

                $pages_data[$k]['id'] = $page_id;
                $pages_data[$k]['permalink'] = esc_url(get_permalink($page_id));

                add_post_meta($page_id, '_generate-sidebar-layout-meta', 'no-sidebar');
                //add_post_meta($page_id, '_generate-full-width-content', 'true');
                //add_post_meta($page_id, '_generate-disable-headline', 'true');

                if (!$page_on_front) {
                    $page_on_front = true;
                    update_option('page_on_front', $page_id);
                }
            }

            foreach ($pages_data as $k => $p) {

                $content = file_get_contents($p['path']);

                // replace images urls
                foreach ($images_data as $img) {
                    $content = str_replace($images_prefix . $img['search'], $img['replace'], $content);
                }

                foreach ($pages_data as $kk => $pp) {
                    $content = str_replace('href="' . $pages_prefix . $kk . '"', 'href="' . $pp['permalink'] . '"', $content);
                }

                $postarr = [
                    'ID' => $p['id'],
                    'post_content' => $content
                ];

                wp_update_post($postarr, false, false);

            }


            $this->create_page_for_posts();

            $this->rrmdir($import_path);

            return true;
        }


        /**
         * Get pages from menu
         * 
         * @param array|object $menu menu array from commons.json
         * @param string $path pages directory
         * @param string|int $parent parent page name or 0
         * @param array $pages pages array to return (optional)
         * 
         * @return array pages array
         */
        private function get_menu_pages($menu, $path, $parent = 0, &$pages = array()) {

            foreach ($menu as $item) {
                $pages[$item['page']] = array(
                    'title' => $item['title'],
                    'path' => $path . DIRECTORY_SEPARATOR . $item['page'] . '.html',
                    'parent' => $parent
                );

                if (isset($item['items'])) {
                    $this->get_menu_pages($item['items'], $path, $item['page'], $pages);
                }
            }

            return $pages;
        }


        /**
         * Scan files in directory with optional filter
         * 
         * @param string $dir directory to scan
         * @param string $filter regular expression
         * @param array $results array to return (optional)
         * 
         * @return array results array
         */
        private function scan_files($dir, $filter = '', &$results = array()) {

            if (!is_dir($dir)) return $results;

            $files = array_diff(scandir($dir), array('.', '..'));

            foreach ($files as $key => $value) {
                $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
                if (!is_dir($path)) {
                    if (empty($filter) || preg_match($filter, $path)) $results[] = $path;
                } else {
                    $this->scan_files($path, $filter, $results);
                }
            }

            return $results;
        }


        /**
         * Remove directory recursively
         * @param string $dir directory to remove
         */
        private function rrmdir($dir) {
            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
                            $this->rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                        else
                            unlink($dir . DIRECTORY_SEPARATOR . $object);
                    }
                }
                rmdir($dir);
            }
        }

        /**
         * Get image extension from mime type
         * @param string $imagetype image mime type
         * 
         * @return string extension
         */
        private function get_img_extension($imagetype) {
            if (empty($imagetype)) return false;
            switch ($imagetype) {
                case 'image/bmp':
                    return 'bmp';
                case 'image/cis-cod':
                    return 'cod';
                case 'image/gif':
                    return 'gif';
                case 'image/ief':
                    return 'ief';
                case 'image/jpeg':
                    return 'jpg';
                case 'image/pipeg':
                    return 'jfif';
                case 'image/tiff':
                    return 'tif';
                case 'image/x-cmu-raster':
                    return 'ras';
                case 'image/x-cmx':
                    return 'cmx';
                case 'image/x-icon':
                    return 'ico';
                case 'image/x-portable-anymap':
                    return 'pnm';
                case 'image/x-portable-bitmap':
                    return 'pbm';
                case 'image/x-portable-graymap':
                    return 'pgm';
                case 'image/x-portable-pixmap':
                    return 'ppm';
                case 'image/x-rgb':
                    return 'rgb';
                case 'image/x-xbitmap':
                    return 'xbm';
                case 'image/x-xpixmap':
                    return 'xpm';
                case 'image/x-xwindowdump':
                    return 'xwd';
                case 'image/png':
                    return 'png';
                case 'image/x-jps':
                    return 'jps';
                case 'image/x-freehand':
                    return 'fh';
                default:
                    return false;
            }
        }


        /**
         * Create page for posts (blog)
         */
        private function create_page_for_posts() {
            $postarr = [
                'post_title' => __('Blog', 'wemcor-multisite'),
                'post_content' => '',
                'post_status' => 'publish',
                'post_type' => 'page'
            ];
            $page_for_posts = wp_insert_post($postarr, false, false);
            update_option('page_for_posts', $page_for_posts);
        }


        /**
         * Create page on front (default)
         */
        private function create_page_on_front() {
            $postarr = [
                'post_title' => __('Pàgina d\'inici', 'wemcor-multisite'),
                'post_content' => '',
                'post_status' => 'publish',
                'post_type' => 'page'
            ];
            $page_on_front = wp_insert_post($postarr, false, true);
            update_option('page_on_front', $page_on_front);
        }


        /**
         * Preproccess blocks to  
         * @param string $content
         * @param callable $callback
         *
         * @return string
         */
        private function preprocess_blocks($content, $callback) {
            $new_content = $content;

            if (has_blocks($content)) {
                $blocks       = parse_blocks($content);
                $parsed_blocks = $this->parse_blocks($blocks, $callback);
                $new_content = serialize_blocks($parsed_blocks);
            }

            return $new_content;
        }
        

        /**
         * Parse blocks
         * @param \WP_Block_Parser_Block[]|array[] $blocks
         * @param callable $callback
         *
         * @return \WP_Block_Parser_Block[]
         */
        private function parse_blocks($blocks, $callback): array {
            $all_blocks = [];

            foreach ($blocks as $block) {
                // Go into inner blocks and run this method recursively
                if (!empty($block['innerBlocks'])) {
                    $block['innerBlocks'] = $this->parse_blocks($block['innerBlocks'], $callback);
                }

                // Make sure that is a valid block (some block names may be NULL)
                if (!empty($block['blockName'])) {
                    $all_blocks[] = $callback($block); // the magic is here...
                    continue;
                }

                // Continuously create back the blocks array.
                $all_blocks[] = $block;
            }

            return $all_blocks;
        }


        /**
         * Aux function to replace last ocurrence in a string
         * 
         * @param string $search
         * @param string $replace
         * @param string $subject
         * 
         * @return string
         */
        private function str_lreplace($search, $replace, $subject) {
            $pos = strrpos($subject, $search);

            if ($pos !== false) {
                $subject = substr_replace($subject, $replace, $pos, strlen($search));
            }

            return $subject;
        }


        /**
         * Aux function to extract title from html content
         * 
         * @param string $content html content
         * @param string $name default name
         * 
         * @return string title
         */
        private function extract_title($content = '', $name = 'Untitled') {
            if (empty($content)) return ucfirst($name);
            $dom = new DOMDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML($content);
            $dom->preserveWhiteSpace = false;
            $headings = $dom->getElementsByTagName('h1');
            if ($headings->length > 0) {
                return strip_tags($headings->item(0)->nodeValue);
            } else {
                return ucfirst($name);
            }
        }
    }

    \WP_CLI::add_command('dd', 'DD_CLI');
    
}
