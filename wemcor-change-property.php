<?php
/*
Plugin Name: Wemcor Change property site
Plugin URI:
Description: Sistema de configuración para cambviar el propietario de un sitio
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_menu', 'wemcor_add_menu_change_property', 10 );
function wemcor_add_menu_change_property() {
	add_menu_page(
		__('Change property', 'wemcor-multisite'),
		__('Change property', 'wemcor-multisite'),
		'read',
		'change-property',
		'wemcor_change_property_callback',
		'dashicons-controls-repeat',
		4
	);
}

function wemcor_change_property_callback() {
	$id = $_REQUEST['id'];
	$current_blog_details = get_blog_details( array( 'blog_id' => $id ) );
	$site_name = $current_blog_details->blogname;

	$property_user_id = function_exists('get_blog_option') ? get_blog_option( $id, 'owner_user' ) : get_option('owner_user');
	$property = get_userdata( $property_user_id );

	if( isset($_POST['submit']) && isset($_POST['id']) && isset($_POST['prop']) ) {
		wp_verify_nonce( $_POST['nonce'], 'wemcor-change-property' . $_POST['id']);

		if( isset($_POST['id']) && wp_verify_nonce( $_POST['nonce'], 'wemcor-change-property' . $_POST['id']) ) {
			$user = get_user_by('login', $_POST['prop']);
			if( $user ) {
				$user_id = $user->ID;

				//change property
				update_blog_option( $id, 'owner_user', $user_id );
				update_blog_option( $id, 'owner_user_old', $property_user_id );
				//owner user role
				$user_meta = get_userdata($user_id);
			    $user_roles = $user_meta->roles;
				//set site to property if not assign
				if( !is_user_member_of_blog($user_id, $id) ) add_user_to_blog($id, $user_id, $user_roles[0]);
				unset($_POST['id']);
				unset($_POST['nonce']);
			}
		}

		echo '<div id="message" class="updated notice is-dismissible"><p>'. $site_name .' '. __('Property has changed successfully to', 'wemcor-multisite') .' <b>'. $user->display_name.'</b></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">'. __('Dismiss this notice', 'wemcor-multisite').'</span></button></div>';

	}

	echo '<div class="wrap"><h1 id="edit-site">' . __('Change property', 'wemcor-multisite') . ' ' . $site_name .'</h1>';
	echo '<p class="edit-site-actions"><a href="' . esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) . 'admin.php?page=mis-sitios">'. __('Back to the View my sites', 'wemcor-multisite') .'</a></p>';
	echo '<form method="post">';
	wp_nonce_field( 'wemcor-change-property' . $id, 'nonce' );
	if( !isset($_POST['submit']) && !isset($_POST['id']) ) {
		echo '<p>'. __('Choose a user and press the Change button to assign ownership of the site. From', 'wemcor-multisite').' <b>'. $property->display_name .'</b>...</p>';
		echo '<input type="hidden" name="id" value="' . $id . '" />';
		get_users_from_json();
		submit_button(__('Change', 'wemcor-multisite'));
	}
	echo '</form></div>';
}

function get_users_from_json() {
	$url = wemcor_get_api_internal_url('users');
	$users = json_decode(@file_get_contents($url), true);
	?>
	<table class="wp-list-table widefat fixed striped table-view-list assigneupropietari">
		<thead>
			<tr>
				<th scope="col" id="id" class="manage-column column-id column-primary">Id</th>
				<th scope="col" id="first" class="manage-column column-first">Nom</th>
				<th scope="col" id="last" class="manage-column column-last">Cognom</th>
				<th scope="col" id="email" class="manage-column column-email">Correu electrònic</th>
				<th scope="col" id="role" class="manage-column column-role">Rol</th>
				<th scope="col" id="groups" class="manage-column column-groups">Grups</th>
				<th scope="col" id="assign" class="manage-column column-assign">Assigneu com a propietari</th>
			</tr>
		</thead>
		<tbody>
	<?php
	if ($users){
		foreach( $users as $user ) {
			if( $user['role'] == 'manager' ) continue; ?>
			<tr>
				<td class="id"><?php echo $user['id']; ?></td>
				<td class="first"><?php echo $user['first']; ?></td>
				<td class="last"><?php echo $user['last']; ?></td>
				<td class="email"><?php echo $user['email']; ?></td>
				<td class="role"><?php echo $user['role']; ?></td>
				<td class="groups"><?php echo implode(', ', $user['groups']); ?></td>
				<td class="assign"><input type="radio" name="prop" value="<?php echo $user['id']; ?>"></td>
			</tr>
		<?php 
		}
	} ?>
		<tfoot>
			<tr>
				<th scope="col" id="id" class="manage-column column-id column-primary">Id</th>
				<th scope="col" id="first" class="manage-column column-first">Nom</th>
				<th scope="col" id="last" class="manage-column column-last">Cognom</th>
				<th scope="col" id="email" class="manage-column column-email">Correu electrònic</th>
				<th scope="col" id="role" class="manage-column column-role">Rol</th>
				<th scope="col" id="groups" class="manage-column column-groups">Grups</th>
				<th scope="col" id="assign" class="manage-column column-assign">Assigneu com a propietari</th>
			</tr>
		</tfoot>
	</table>
	<?php
}


// Validaciones
//add_action( 'current_screen', 'wemcor_redirects_change_property' );
function wemcor_redirects_change_property() {
	$screen = get_current_screen();
	if( $screen->id !== 'sites_page_changeproperty-network' ) {
		return;
	}

	$id = isset( $_REQUEST['id'] ) ? intval( $_REQUEST['id'] ) : 0;

	if ( ! $id ) {
		wp_die( __('Invalid site ID.', 'wemcor-multisite') );
	}

	$details = get_site( $id );
	if ( ! $details ) {
		wp_die( __( 'The requested site does not exist.', 'wemcor-multisite' ) );
	}
}

// Esconder menu en barra lateral (ya se muestra en tabs de site info)
add_action( 'admin_head', 'wemcor_hide_change_property' );
function wemcor_hide_change_property() {
	echo '<style>#adminmenu li.toplevel_page_change-property { display: none; }</style>';
}

add_action( 'admin_head', 'wemcor_owners_admin_head' );
function wemcor_owners_admin_head() { ?>
	<style>
	table.assigneupropietari #assign,
	table.assigneupropietari .assign {
		text-align: center!important;
	}
	</style>
<?php
}

