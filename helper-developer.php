<?php
/*
Plugin Name: Helper Developer
Plugin URI:
Description: Funciones de ayuda para el desarrollo
Author:
Author URI:
Text Domain:
*/

//Helper debug
function jsm($string) {
	echo '<pre>';
  	print_r( $string );
  	echo '</pre>';
}



/**
 * Pretty Printing
 *
 * @param mixed $obj
 * @param string $label
 * @return null
 */
function dd_pp( $obj, $label = '' ) {
	$data = json_encode( print_r( $obj,true ) );
	?>
	<style type="text/css">
	#dd-logger {
      position: absolute;
      top: 0;
      right: 0;
      bottom:0;
      border-left: 4px solid #bbb;
      padding: 1rem;
      background: white;
      color: #444;
      z-index: 999;
      font-size: 1rem;
      width: 25%;
      height: 100%;
      overflow: scroll;
	}
	</style>
	<script type="text/javascript">
		var doStuff = function(){
			var obj = <?php echo $data; ?>;
			var logger = document.getElementById('dd-logger');
			if (!logger) {
				logger = document.createElement('div');
				logger.id = 'dd-logger';
				document.body.appendChild(logger);
			}
			// console.log(obj);
			var pre = document.createElement('pre');
			var h2 = document.createElement('h2');
			pre.innerHTML = obj;
			h2.innerHTML = '<?php echo addslashes($label); ?>';
			logger.appendChild(h2);
			logger.appendChild(pre);
		};
		window.addEventListener ("DOMContentLoaded", doStuff, false);
	</script>
	<?php
}


/**
 * Display Post Blocks 
 */
function dd_display_post_blocks() {
	global $post;
	dd_pp( esc_html( $post->post_content ) );
}
// add_action( 'wp_footer', 'dd_display_post_blocks' );
