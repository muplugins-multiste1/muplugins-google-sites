<?php

/**
 * Plugin Name:     DD Customize
 * Plugin URI:      https://tresipunt.com/
 * Description:     Digital Democratic Customizer Settings
 * Author:          3ipunt
 * Author URI:      https://tresipunt.com/
 * Text Domain:     dd
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         DD
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

class DD_Customize {
    
	/**
	 * Singleton class instance
	 *
	 * @access   private static
	 * @var      object    $instance    Class instance
	 */
	private static $instance = null;

	/**
	 * The current version of the plugin.
	 * 
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version = '1.0.0';

    /**
     * Customizer DD settings base key
     * 
	 * @access   protected
	 * @var      string    $customize_key
     */
	protected $option_key = 'dd_editor_blocks';

    /**
     * Protected blocks by category with defaults
     * Set default enabled blocks in 'defaults' array for each category
     * 
	 * @access   protected
	 * @var      array    $blocks
     */
    protected $blocks = array (
        'generateblocks' => array ( 
            'title'    => 'GenerateBlocks',
            'defaults' => array(
                'generateblocks/button-container',
                'generateblocks/container',
                'generateblocks/grid',
                'generateblocks/headline',
            ),
            'blocks'   => array(
                'generateblocks/button-container'   => 'Buttons',
                'generateblocks/container'          => 'Container',
                'generateblocks/grid'               => 'Grid',
                'generateblocks/headline'           => 'Headline',
            ),
        ),
        'text' => array( 
            'title'    => 'Text',
            'defaults' => array (
                'core/heading',
                'core/list',
                'core/paragraph',
                'core/table',
            ),
            'blocks'   => array (
                'core/code'                         => 'Code',
                'core/freeform'                     => 'Classic',
                'core/heading'                      => 'Heading',
                'core/list'                         => 'List',
                'core/paragraph'                    => 'Paragraph',
                'core/preformatted'                 => 'Preformatted',
                'core/pullquote'                    => 'Pullquote',
                'core/quote'                        => 'Quote',
                'core/table'                        => 'Table',
                'core/verse'                        => 'Verse',
            ),
        ),
        'media' => array ( 
            'title'    => 'Media',
            'defaults' => array (
                'core/audio',
                'core/cover',
                'core/file',
                'core/gallery',
                'core/image',
                'core/video',
            ),
            'blocks'   => array (
                'core/audio'                        => 'Audio',
                'core/cover'                        => 'Cover',
                'core/file'                         => 'File',
                'core/gallery'                      => 'Gallery',
                'core/image'                        => 'Image',
                'core/media-text'                   => 'Media & Text',
                'core/video'                        => 'Video',
            ),
        ),
        'design' => array( 
            'title'    => 'Design',
            'defaults' => array(
                'core/buttons',
                'core/columns',
                'core/group',
                'core/more',
                'core/separator',
                'core/spacer',
            ),
            'blocks'   => array(
                'core/buttons'                      => 'Buttons',
                'core/columns'                      => 'Columns',
                'core/group'                        => 'Group',
                'core/more'                         => 'More',
                'core/separator'                    => 'Separator',
                'core/spacer'                       => 'Spacer',
            ),
        ),
        'widgets' => array ( 
            'title'    => 'Widgets',
            'defaults' => array (
                'core/page-list',
                'core/search',
                'core/social-links',
            ),
            'blocks'   => array (
                'core/archives'                     => 'Archives',
                'core/calendar'                     => 'Calendar',
                'core/categories'                   => 'Categories',
                'core/html'                         => 'Custom HTML',
                'core/latest-comments'              => 'Latest Comments',
                'core/latest-posts'                 => 'Latest Posts',
                'core/page-list'                    => 'Page List',
                'core/rss'                          => 'RSS',
                'core/search'                       => 'Search',
                'core/shortcode'                    => 'Shortcode',
                'core/social-links'                 => 'Social Icons',
                'core/tag-cloud'                    => 'Tag Cloud',
            ),
        ),
        'theme' => array ( 
            'title'    => 'Theme',
            'defaults' => array (),
            'blocks'   => array (
                'core/loginout'                     => 'Login/out',
                'core/navigation'                   => 'Navigation',
                'core/pattern'                      => 'Pattern',
                'core/post-author'                  => 'Post Author',
                'core/post-comments'                => 'Post Comments',
                'core/post-content'                 => 'Post Content',
                'core/post-date'                    => 'Post Date',
                'core/post-excerpt'                 => 'Post Excerpt',
                'core/post-featured-image'          => 'Post Featured Image',
                'core/post-navigation-link'         => 'Post Navigation Link',
                'core/post-terms'                   => 'Post Terms',
                'core/post-title'                   => 'Post Title',
                'core/query'                        => 'Query Loop',
                'core/query-title'                  => 'Query Title',
                'core/site-logo'                    => 'Site Logo',
                'core/site-tagline'                 => 'Site Tagline',
                'core/site-title'                   => 'Site Title',
                'core/template-part'                => 'Template Part',
                'core/term-description'             => 'Term Description',
            ),
        ),
        'embeds' => array ( 
            'title'    => 'Embeds',
            'defaults' => array ( 
                'youtube',
            ),
            'blocks'   => array (
                'amazon-kindle'                     => 'Amazon Kindle',
                'animoto'                           => 'Animoto',
                'cloudup'                           => 'Cloudup',
                'crowdsignal'                       => 'Crowdsignal',
                'dailymotion'                       => 'Dailymotion',
                'flickr'                            => 'Flickr',
                'imgur'                             => 'Imgur',
                'issuu'                             => 'Issuu',
                'kickstarter'                       => 'Kickstarter',
                'mixcloud'                          => 'Mixcloud',
                'pinterest'                         => 'Pinterest',
                'reddit'                            => 'Reddit',
                'reverbnation'                      => 'ReverbNation',
                'screencast'                        => 'Screencast',
                'scribd'                            => 'Scribd',
                'slideshare'                        => 'Slideshare',
                'smugmug'                           => 'SmugMug',
                'soundcloud'                        => 'SoundCloud',
                'speaker-deck'                      => 'Speaker Deck',
                'spotify'                           => 'Spotify',
                'ted'                               => 'TED',
                'tiktok'                            => 'TikTok',
                'tumblr'                            => 'Tumblr',
                'twitter'                           => 'Twitter',
                'videopress'                        => 'VideoPress',
                'vimeo'                             => 'Vimeo',
                'wolfram-cloud'                     => 'Wolfram Cloud',
                'wordpress'                         => 'WordPress',
                'wordpress-tv'                      => 'WordPress.tv',
                'youtube'                           => 'YouTube',
            ),
        ),
    );


	/**
	 * Return an instance of the class
	 *
	 * @return DD_Customize class instance.
	 * @access public static
	 */
	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	/**
	 * Constructor function to register plugin text domain, actions and filters
	 */
	public function __construct() {
        $this->hooks();
	}


	/**
     * Setup plugin hooks
     *
     * @access      private
     * @return      void
     */
    private function hooks() {

        // Localization
        add_action( 'init',			                array( $this, 'localization' ) );

		// Register customizer settings
		add_action( 'customize_register',           array( $this, 'customize_register' ) );

        // Blacklist blocks hook (not working for embeds)
        add_filter( 'allowed_block_types_all',      array( $this, 'whitelist_blocks') );

        // Enqueue block editor asssets (unregister disabled oembed providers)
        add_action( 'enqueue_block_editor_assets',  array( $this, 'block_editor_assets') );

        // Whitelist oembed providers (not working)
        // add_filter( 'oembed_providers',             array ( $this, 'whitelist_embed_providers' ), 99 );
    }

    /**
     * Initialize plugin for localization
     *
     * @uses load_plugin_textdomain()
     */
    public function localization() {
        load_muplugin_textdomain( 'dd', 'languages' );
    }

    /**
     * Customize register DD settings
     *
     * @param WP_Customize_Manager $wp_customize Theme Customizer object.
     */
    function customize_register( $wp_customize ) {

        require_once plugin_dir_path( __FILE__ ) . 'includes/dd/dd-customize-control-checkbox-multiple.php';

        $wp_customize->add_section( 'dd_editor_blocks' , array (
            'title' 		=> __( 'Editor Blocks Settings', 'dd'),
            'description' 	=> __( 'Enable/Disable Editor Blocks.', 'dd'),
            'capability' 	=> 'edit_theme_options',
            'priority'		=> 1
        ) );

        foreach ( $this->blocks as $ck => $category ) {

            // Translate block category
            $label = isset($category['title']) ? _x( $category['title'], 'block category' ) : $ck;

            // Translate block titles
            $choices = isset($category['blocks']) ? $category['blocks'] : array();
            foreach ( $choices as $k => $choice ) {
                $choices[$k] = _x( $choice, 'block title' );
            }

            // Add setting for each category
            $wp_customize->add_setting( 
                $this->option_key . '['. $ck .']',
                array (
                    'default' 		    => isset($category['defaults']) ? $category['defaults'] : array(),
                    'transport'         => 'postMessage',
                    'sanitize_callback' => array( $this, 'sanitize_multiple_checkbox' ),
                ) 
            );

            // Add multiple checkbox control for each category
            $wp_customize->add_control(
                new DD_Customize_Control_Checkbox_Multiple(
                    $wp_customize,
                    $this->option_key . '_' . $ck,
                    array(
                        'section'   => 'dd_editor_blocks',
                        'settings'  => $this->option_key . '[' . $ck . ']',
                        'label'     => $label,
                        'choices'   => $choices,
                    )
                )
            );

        }
    }

    /**
     * Multiple checkbox sanitization callback
     *
     * @param array|string $values
     * @return array
     */ 
    public function sanitize_multiple_checkbox( $values ) {
        $multi_values = !is_array( $values ) ? explode( ',', $values ) : $values;
        return !empty( $multi_values ) ? array_map( 'sanitize_text_field', $multi_values ) : array();
    }

    /**
     * Block Editor DD Assets
     * 
     * @return void
     */
    public function block_editor_assets() {
        $enabled_blocks = get_theme_mod( $this->option_key, array() );

        // Fill with defaults for each category if not set
        foreach ( $this->blocks as $ck => $category ) {
            if ( ! isset( $enabled_blocks[ $ck ] ) ){
               $enabled_blocks[ $ck ] = isset( $category['defaults'] ) ? $category['defaults'] : array();
            }
        }

        // Register scripts
        wp_register_script(
            'dd-blocks',
            plugin_dir_url( __FILE__ ) . '/assets/js/dd-blocks.js',
            array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
            $this->version,
            true
        );

        // localization data
        wp_localize_script( 'dd-blocks', 'dd_blocks_settings', array (
            'blocks' => $enabled_blocks
        ) );

        // Enqueue scripts
		wp_enqueue_script( 'dd-blocks' );
    }

     
    /**
     * Filter whitelisted editor blocks
     * 
     * @param bool|string[] $allowed_blocks
     * @return string[]
     */
    public function whitelist_blocks( $allowed_blocks ) {
        $whitelist = array();
        $enabled_blocks = get_theme_mod( $this->option_key, array() );

        // Fill with defaults for each category if not set
        foreach ( $this->blocks as $key => $category ) {
            if ( ! isset( $enabled_blocks[ $key ] )){
               $enabled_blocks[ $key ] = isset( $category['defaults'] ) ? $category['defaults'] : array();
            }
        }

        // Whitelist enabled blocks + 'core/embed'
        // NOTE: if 'core/embed' not whitelisted dont allow paste urls or iframes from providers
        foreach ( $enabled_blocks as $key => $category ) {
            $values = ! is_array( $category ) ? explode( ',', $category ) : $category;
            $values = array_filter($values);
            if ( $key === 'embeds') { // embeds category 
                $whitelist[] = 'core/embed';
            }else{ // another category
                foreach ( $values as $value ) {
                    $whitelist[] = $value;
                }
            } 
        }

        return $whitelist;
    }

    /**
     * Filter the oembed providers through a whitelist 
     * (not used because not working)
     *
     * @param array $providers
     * @return array
     */
    public function whitelist_embed_providers( $providers ) {
        $output = array();
        $enabled_blocks = get_theme_mod( $this->option_key, array() );
        $whitelist = isset( $enabled_blocks['embeds'] ) ? $enabled_blocks['embeds'] : array();
        $whitelist = ! is_array( $whitelist ) ? explode( ',', $whitelist ) : $whitelist;
        $whitelist = array_filter($whitelist);

        if( count($whitelist) ) {
            foreach( $providers as $key => $provider ) {
                foreach( $whitelist as $allowed ) {
                    if( stristr( $key, $allowed ) ) {
                        $output[$key] = $provider;
                    }
                }
            }
            return $output; 
        }

        return $providers;
    }

}

add_action('plugins_loaded', 'dd_customize_instantiate');

$dd_customize = null;
function dd_customize_instantiate() {
	global $dd_customize;
	$dd_customize = DD_Customize::get_instance();
}
