<?php
/*
Plugin Name: Wemcor Admin Bar
Plugin URI:
Description: Barra de administración de Wordpress personalizada para mostrar estilos diferentes y menús de navegación a medida. Compatible con multisite
Author: Wemcor
Author URI: https://wemcor.com
Version: 3.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//definimos variables globales
add_action( 'init', 'wemcor_global_variables' );
function wemcor_global_variables() {
	//GLOBAL VARIABLES
	global $bg_logo;
	global $bg_color;
	global $primary;
	global $secondary;
	global $product_logo;
	global $product_url;

	if ( DD_DEMO_MODE && defined( 'WP_CLI' ) && WP_CLI ) return false;

	//JSON
    $path_json = wemcor_get_url_json();
    $json = @file_get_contents($path_json);

	if (!$json) return false;

	//echo '<pre>' . var_export($json, true) . '</pre>';
	$json_data = json_decode($json, true);

	if( isset($json_data['logo']) ) $bg_logo = $json_data['logo'];
	else $bg_logo = WPMU_PLUGIN_URL . '/custom-logo-admin-bar.png';

	if( isset($json_data['colours']['background']) ) $bg_color = $json_data['colours']['secondary'];
	else $bg_color = '#FFF';

	if( isset($json_data['colours']['primary']) ) $primary = $json_data['colours']['primary'];
	else $primary = '#262626';

	if( isset($json_data['colours']['secondary']) ) $secondary = $json_data['colours']['background'];
	else $secondary = '#f0f0f0';

	if( isset($json_data['product_logo']) ) $product_logo = $json_data['product_logo'];
	else $product_logo = WPMU_PLUGIN_URL . '/assets/dd.svg';

	if( isset($json_data['product_url']) ) $product_url = $json_data['product_url'];
	else $product_url = 'https://xnet-x.net/ca/digital-democratic/';
}

//añadimos items de menú personalizados
add_action( 'admin_bar_menu', 'wemcor_add_new_items_admin_bar', 999 );
function wemcor_add_new_items_admin_bar($admin_bar) {
	global $bg_logo;
	global $product_logo;
	global $product_url;

    //Logo
    $args = array(
        'id'    => 'wp-logo',
        'title' => '<img src="'.$bg_logo.'">',
        'href'  => admin_url() . 'admin.php?page=mis-sitios'
    );
    $admin_bar->add_node( $args );

    //digital democratic
    $args = array(
        'parent' => 'top-secondary',
        'id'     => 'digital-democratic',
        'title'  => false,
        'href'   => $product_url,
        'meta'   => array(
        	'class' => 'digital-democratic',
        	'target' => '_blank',
        	'html' => ''
        )
    );
    $admin_bar->add_node( $args );

	//apps
    $html = html_menu_apps();
	$args = array(
        'parent' => 'top-secondary',
        'id'     => 'dropdown-menu',
        'title'  => false,
        'href'   => '#',
        'meta'   => array(
        	'class' => 'dropdown-menu menupop',
        	'html' => $html
        )
    );
    $admin_bar->add_node( $args );

    //mis sitios
    $current_blog_id = get_current_blog_id();
    $blogname = function_exists('get_blog_option') ? get_blog_option( $current_blog_id, 'blogname') : get_option('blogname');
	$args = array(
        'parent' => '',
        'id'     => 'mis-sitios',
        'title'  => __('My websites', 'wemcor-multisite') . ' ('. $blogname .')',
        'href'   => admin_url() . 'admin.php?page=mis-sitios',
        'meta'   => array(
        	'class' => 'mis-sitios'
        )
    );
    $admin_bar->add_node( $args );

}

//borramos todos los items de admin bar excepto los nuevos dados de alta en el paso anterior y logo y usuario que lo modificaremos posteriormente. De esta forma prevenimos la carga de ítems extras colocados por otros plugins que se puedan instalar en un futuro
add_action( 'wp_before_admin_bar_render', 'wemcor_remove_menu_admin_bar', PHP_INT_MAX );
function wemcor_remove_menu_admin_bar() {
    global $wp_admin_bar;

	if (! defined('DOMAIN_CURRENT_SITE') ) return false;

    //JSON
    $path_json = wemcor_get_url_json();
    $json = @file_get_contents($path_json);

	if (!$json) return false;

	$json_data = json_decode($json, true);

    $no_clear_menus = array(
    	'wp-logo',
    	'my-account',
    	'user-actions',
    	'logout',
    	'menu-toggle',
    	'dropdown-menu',
    	'mis-sitios',
    	'top-secondary',
    	'digital-democratic',
    	'notifications'
	);

	$all_menus = $wp_admin_bar->get_nodes();
	remove_nodes($no_clear_menus, $all_menus, $wp_admin_bar);

	// current user
	$user_id = get_current_user_id();
	$user_data = get_user_by( 'id', $user_id );
	$user_name = $user_data->display_name;

	// modificación de ítems existentes
	$my_account = $wp_admin_bar->get_node( 'my-account' );
	if($my_account) {
		$html_user = html_menu_user();
		$my_account->meta['html'] = $html_user;
		$my_account->meta['class'] .= ' dropdown-menu';

		if( isset($json_data['user_avatar']) ) {
			//$avatar = wp_remote_get($json_data['user']['avatar']);
			//if( $avatar['response']['code'] == '200') $user_avatar = '<img src="'.$json_data['user']['avatar'].'"/>';
			//else $user_avatar = '<img src="'.plugin_dir_url(__FILE__) . 'assets/avatar-default.png"/>';
			$user_avatar = '<img src="'.$json_data['user']['avatar'].'"/>';
		} else {
			$user_avatar = '<img src="'.plugin_dir_url(__FILE__) . 'assets/avatar-default.png"/>';
		}

		$wp_admin_bar->add_node(
			array(
	            'parent'    => $my_account->parent,
	            'id'        => $my_account->id,
	            'title'     => $user_avatar,
	            'href'      => '#',
	            'group' 	=> $my_account->group,
	            'meta' 		=> $my_account->meta
				
    		)
		);

		$logout_node = $wp_admin_bar->get_node( 'logout' );
		$logout_wp_nonce = explode('&', $logout_node->href);
		$count_explode = count($logout_wp_nonce) - 1;
		$href_logout = 'https://sso.'.ltrim(DOMAIN_CURRENT_SITE, 'wp.').'/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https://wp.'.ltrim(DOMAIN_CURRENT_SITE, 'wp.').'&'.$logout_wp_nonce[$count_explode];
		//https://sso.DOMAIN_CURRENT_SITE/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https%3A%2F%2Fwp.DOMAIN_CURRENT_SITE
		//https://sso.subdomain.domain.com/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https://wp.subdomain.domain.com&_wpnonce=5b2c887063
		//https://wp.subdomain.domain.com/wp-login.php?action=logout&_wpnonce=5b2c887063

		$wp_admin_bar->add_node(
			array(
	            'parent'    => $logout_node->parent,
	            'id'        => $logout_node->id,
	            'title'     => $logout_node->title,
	            'href'      => $href_logout,
	            'group' 	=> $logout_node->group,
	            'meta' 		=> $logout_node->meta
    		)
		);
		$wp_admin_bar->remove_node('logout');

	}

}

function remove_nodes($no_clear_menus, $all_menus, $wp_admin_bar) {
	foreach( $all_menus as $menu ) {
    	if( ! in_array($menu->id, $no_clear_menus) ) $wp_admin_bar->remove_menu( $menu->id );
    }
}


//estilos personalizados admin bar
add_action( 'wp_before_admin_bar_render', 'wemcor_custom_styles_admin_bar', 999 );
function wemcor_custom_styles_admin_bar() {
	// //GLOBAL VARIABLES
	global $bg_logo;
	global $bg_color;
	global $primary;
	global $secondary;

	// JSON
 	// $path_json = wemcor_get_url_json();
 	// $json = @file_get_contents($path_json);
	// $json_data = json_decode($json, true);

	// if( isset($json_data['logo']) ) $bg_logo = $json_data['logo'];
	// else $bg_logo = WPMU_PLUGIN_URL . '/custom-logo-admin-bar.png';

	// if( isset($json_data['colours']['background']) ) $bg_color = $json_data['colours']['secondary'];
	// else $bg_color = '#FFF';

	// if( isset($json_data['colours']['primary']) ) $primary = $json_data['colours']['primary'];
	// else $primary = '#262626';

	// if( isset($json_data['colours']['secondary']) ) $secondary = $json_data['colours']['background'];
	// else $secondary = '#f0f0f0';

	require_once dirname(__FILE__) . '/assets/admin-bar-style.php';

}

//encolamos javascript para funciones de menu toggle
add_action( 'admin_enqueue_scripts', 'wemcor_admin_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'wemcor_admin_enqueue_scripts' );
function wemcor_admin_enqueue_scripts() {

	//FontAwesome 5
	//wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css', '', '5.8.1', 'all');

	//FontAwesome 4
	wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', '', '4.7.0', 'all');
	
	// Montserrat Font Family
	//wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap', false );

	$plugin_url = plugin_dir_url( __FILE__ );
	wp_register_script('dd-disclosure-menu', $plugin_url.'assets/js/disclosure-menu.min.js', array(), '1.0', true );
    wp_enqueue_script('dd-disclosure-menu');
    wp_register_script('dd-adminbar-menu', $plugin_url.'assets/js/admin-bar-menu.js', array('jquery', 'dd-disclosure-menu'), '1.0', true );
    wp_enqueue_script('dd-adminbar-menu');

}

//HTML del menú de apps
function html_menu_apps() {
	//JSON
    $path_json = wemcor_get_url_json();
    $json = @file_get_contents($path_json);

	if (!$json) return false;

    $json_data = json_decode($json, true);

	/*
	Array(
	    [background_login] => https://api.subdomain.domain.com/custom/img/background.png
	    [colours] => Array(
	        [background] => #F0F0F0
	        [primary] => #92AE01
	        [secondary] => #FFFFFF)
	    [logo] => https://api.subdomain.domain.com/custom/img/logo.png
	    [apps_external] => Array(
	        [0] => Array(
	            [href] => https://.../
	            [icon] => fa fa-university
	            [name] => Escola Web
	            [shortname] => web
	        )
            [1] => Array(
                [href] => https://.../
                [icon] => fa fa-youtube-play
                [name] => Youtube
                [shortname] => youtube)
            [2] => Array(
                [href] => https://.../
                [icon] => fa fa-book
                [name] => Diccionari
                [shortname] => diccionari)
            [3] => Array(
                [href] => http://meet.jit.si/
                [icon] => fa fa-video-camera
                [name] => Reunions Jitsi
                [shortname] => jitsi)
            [4] => Array(
                [href] => https://.../
                [icon] => fa fa-search
                [name] => Cercar
                [shortname] => search)
            [5] => Array(
                [href] => https://...
                [icon] => fa fa-map-marker
                [name] => Maps
                [shortname] => maps)
	    )
	    [apps_internal] => Array(
            [0] => Array(
                [href] => https://.../
                [icon] => fa fa-cloud
                [name] => Núvol + crear arxius
                [shortname] => cloud)
            [1] => Array(
                [href] => https://...
                [icon] => fa fa-envelope-o
                [name] => Correu
                [shortname] => email)
            [2] => Array(
                [href] => https://.../
                [icon] => fa fa-file-text-o
                [name] => Pads
                [shortname] => pads)
            [3] => Array(
                [href] => https://...
                [icon] => fa fa-check-square-o
                [name] => Formularis
                [shortname] => forms)
            [4] => Array(
                [href] => https://...
                [icon] => fa fa-bar-chart
                [name] => Enquestes
                [shortname] => feedback)
            [5] => Array(
                [href] => https://...
                [icon] => fa fa-commenting-o
                [name] => Xat
                [shortname] => chat)
            [6] => Array(
                [href] => https://...
                [icon] => fa fa-calendar
                [name] => Calendari
                [shortname] => schedule)
            [7] => Array(
                [href] => https://wp.subdomain.domain.com/wp-login.php?saml_sso
                [icon] => fa fa-rss
                [name] => Webs
                [shortname] => webs)
            [8] => Array(
                [href] => https://...
                [icon] => fa fa-video-camera
                [name] => Reunions BBB
                [shortname] => meets_bbb)
            [9] => Array(
                [href] => https://...
                [icon] => fa fa-file-image-o
                [name] => Fotos
                [shortname] => photos)
		)
	    [user] => Array(
            [account] => https://sso.subdomain.domain.com/auth/realms/master/account
            [avatar] => https://sso.subdomain.domain.com/auth/realms/master/avatar-provider
            [password] => https://sso.subdomain.domain.com/auth/realms/master/password
        )
	)
	*/
	$html = '';
	if(isset($json_data['apps_external'])) $apps_external = $json_data['apps_external'];
	else $apps_external = false;

	if(isset($json_data['apps_internal'])) $apps_internal = $json_data['apps_internal'];
	else $apps_internal = false;

	if(isset($json_data['apps_courses'])) $apps_courses = $json_data['apps_my_courses'];
	else $apps_courses = false;

	if( $apps_external || $apps_internal || $apps_courses ) $html .= '<div id="wp-admin-bar-menu-apps" class="ab-sub-wrapper">';

	//internals
	if( $apps_internal ) {
		$findme = 'https://wp.';
		$html .= '<ul id="app-apps" class="ab-submenu">';
		foreach($apps_internal as $app) {
			/*
			[0] => Array(
		        [href] => https://.../
		        [icon] => fa fa-university
		        [name] => Escola Web
		        [shortname] => web
		    )
			*/
			//buscamos link de WP para éste no abrirlo en nueva ventana
			$pos = strpos($app['href'], $findme);
			if( $pos === 0 ) $target = '_self';
			else $target = '_blank';

			$html .= '<li data="'.$pos.'" class="'.$app['shortname'].'"><a target="'.$target.'" href="'.$app['href'].'" class="ab-item"><div class="icon"><i class="'.$app['icon'].'"></i></div><div class="text">'.$app['name'].'</div></a></li>';
		}
		$html .= '</ul>';
	}

	//externals
	if( $apps_external ) {
		$html .= '<ul id="app-external" class="ab-submenu">';
		foreach($apps_external as $app) {
			/*
			[0] => Array(
		        [href] => https://.../
		        [icon] => fa fa-university
		        [name] => Escola Web
		        [shortname] => web
		    )
			*/
			$html .= '<li class="'.$app['shortname'].'"><a target="_blank" href="'.$app['href'].'" class="ab-item"><div class="icon"><i class="'.$app['icon'].'"></i></div><div class="text">'.$app['name'].'</div></a></li>';
		}
		$html .= '</ul>';
	}

	//courses
	if( $apps_courses ) {
		$html .= '<ul id="app-external" class="ab-submenu">';
		foreach($apps_courses as $app) {
			/*
			[0] => Array(
		        [href] => https://.../
		        [icon] => fa fa-university
		        [name] => Escola Web
		        [shortname] => web
		    )
			*/
			$html .= '<li class="'.$app['shortname'].'"><a target="_blank" href="'.$app['href'].'" class="ab-item"><div class="icon"><i class="'.$app['icon'].'"></i></div><div class="text">'.$app['name'].'</div></a></li>';
		}
		$html .= '</ul>';
	}

	if( $apps_external || $apps_internal || $apps_courses ) $html .= '</div>';


	return $html;
}

//HTML del menú de usuario
function html_menu_user() {

	global $wp_admin_bar;

	if (! defined('DOMAIN_CURRENT_SITE') ) return false;

	//JSON
    $path_json = wemcor_get_url_json();

    $json = @file_get_contents($path_json);

	if (!$json) return false;

    $json_data = json_decode($json, true);

    $html_user = '';
    /*
	{
	  "user_menu": [
	    {
	      "href": "https://...",
	      "icon": "fa fa-user",
	      "name": "Perfil",
	      "shortname": "profile"
	    },
	    {
	      "href": "https://<app_subdomain>...<app_plugin>",
	      "icon": "fa fa-sign-out",
	      "name": "Sortir",
	      "shortname": "logout"
	    }
	  ],
	  "user_avatar": "https://..."
	}
	*/

	if(isset($json_data['user_menu'])) $user_menu = $json_data['user_menu'];
	else $user_menu = false;

	$logout_node = $wp_admin_bar->get_node( 'logout' );
	$logout_wp_nonce = explode('&', $logout_node->href);
	$count_explode = count($logout_wp_nonce) - 1;

	$html_user .= '<div id="wp-admin-bar-menu-users" class="ab-sub-wrapper">';
	if( $user_menu ) {
		$html_user .= '<ul id="app-users" class="ab-submenu">';
		foreach($user_menu as $key => $item) {
			if ( $item['shortname'] === 'profile' ) {
				$current_user = wp_get_current_user();
				$item['name'] = $current_user->display_name;
			}
			if( $item['shortname'] === 'logout' && ! DD_DEMO_MODE ) {
				$item['href'] = 'https://sso.'.ltrim(DOMAIN_CURRENT_SITE, 'wp.').'/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https://wp.'.ltrim(DOMAIN_CURRENT_SITE, 'wp.').'&'.$logout_wp_nonce[$count_explode];
				$html_user .= '<hr>';
			}
			$html_user .= '<li class="'.$item['shortname'].'"><a href="'.$item['href'].'" class="ab-item"><i class="'.$item['icon'].'"></i><span class="text">'.$item['name'].'</span></a></li>';
		}
		$html_user .= '</ul>';
	}
	$html_user .= '</div>';

	return $html_user;
}

function wemcor_get_url_json() {
	if ( DD_DEMO_MODE ) {
		return plugins_url( 'dd-demo/api/api-admin-bar.json', __FILE__ );
	}
	if ( !defined('DOMAIN_CURRENT_SITE') ){
		return false;
	}
	$path = str_replace('wp.', 'api.', DOMAIN_CURRENT_SITE);
    return 'https://'.$path.'/json';
}

add_action( 'admin_init', 'wemcor_remove_lateral_menu' );
function wemcor_remove_lateral_menu() {
	if( !current_user_can( 'manage_network' ) ) {
		global $menu;
		$menu = array();
	}
}


