<?php
/*
Plugin Name: Wemcor Delete site
Plugin URI:
Description: Sistema de configuración para borrar sitios
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_filter( 'network_edit_site_nav_links', 'wemcor_custom_siteinfo_delete_site_tabs' );
function wemcor_custom_siteinfo_delete_site_tabs( $tabs ) {
	$tabs['delete-site'] = array(
		'label' => __('Delete Site', 'wemcor-multisite'),
		'url' => 'sites.php?page=deletesite',
		'cap' => 'manage_sites'
	);
	return $tabs;
}

add_action( 'network_admin_menu', 'wemcor_page_delete_site' );
function wemcor_page_delete_site() {
	add_submenu_page(
		'sites.php',
		__('Delete Site', 'wemcor-multisite'),
		__('Delete Site', 'wemcor-multisite'),
		'manage_sites',
		'deletesite',
		'callback_delete_site'
	);
}

add_action( 'admin_menu', 'wemcor_add_menu_delete_site', 10 );
function wemcor_add_menu_delete_site() {
	add_menu_page(
		__('Delete site', 'wemcor-multisite'),
		__('Delete site', 'wemcor-multisite'),
		'read',
		'delete-site',
		'wemcor_delete_site_callback',
		'dashicons-trash',
		4
	);
}

function callback_delete_site() {
	$id = $_REQUEST['id'];
	$current_blog_details = get_blog_details( array( 'blog_id' => $id ) );
	$site_name = $current_blog_details->blogname;

	network_edit_site_nav( array(
		'blog_id'  => $id,
		'selected' => 'delete-site'
	) );

	if( $id != BLOG_ID_CURRENT_SITE ) {

		echo '<div class="wrap"><h1 id="edit-site">' . __('Delete Site', 'wemcor-multisite') . ' ' . $site_name .'</h1>';
		echo '<p class="edit-site-actions"><a href="' . esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) . 'admin.php?page=mis-sitios">'. __('Back to the View my sites', 'wemcor-multisite') .'</a></p>';
		echo '<form method="post" action="edit.php?action=deleteweb">';
		wp_nonce_field( 'wemcor-delete-site' . $id, 'nonce' );
		echo '<p>'. __('Confirm that you want to delete the website. If you delete it, all data will be lost. The deletion of the site is irreversible and cannot be recovered.', 'wemcor-multisite') .'</p>';
		echo '<input type="hidden" name="id" value="' . $id . '" />';
		submit_button(__('Delete site', 'wemcor-multisite'));
		echo '</form></div>';

	} else {

		echo '<div class="wrap"><h1 id="edit-site">' . __('Delete Site', 'wemcor-multisite') . '</h1>';
		echo '<p class="edit-site-actions">'. __('Main site not allowed delete', 'wemcor-multisite') .'</a></p>';

	}
}

function wemcor_delete_site_callback() {
	$id = $_REQUEST['id'];
	$current_blog_details = get_blog_details( array( 'blog_id' => $id ) );
	$site_name = $current_blog_details->blogname;

	if( isset($_POST['submit']) && isset($_POST['id']) ) {
		wp_verify_nonce( $_POST['nonce'], 'wemcor-delete-site' . $_POST['id']);

		if( isset($_POST['id']) && wp_verify_nonce( $_POST['nonce'], 'wemcor-delete-site' . $_POST['id']) ) {
			if( isset($_POST['wemcor-deletesite'] ) ) $value = $_POST['wemcor-deletesite'];
			else $value = 0;
			$blog_id = $_POST['id'];

			//delete site
			wp_delete_site( $blog_id );
			unset($_POST['id']);
			unset($_POST['nonce']);
		}

		echo '<div id="message" class="updated notice is-dismissible">
			<p>'. $site_name .' '. __('Site has deleted successfully', 'wemcor-multisite') .'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">'. __('Dismiss this notice', 'wemcor-multisite').'</span></button>
		</div>';

	}

	echo '<div class="wrap"><h1 id="edit-site">' . __('Delete Site', 'wemcor-multisite') . ' ' . $site_name .'</h1>';
	echo '<p class="edit-site-actions"><a href="' . esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) . 'admin.php?page=mis-sitios">'. __('Back to the View my sites', 'wemcor-multisite') .'</a></p>';
	echo '<form method="post">';
	wp_nonce_field( 'wemcor-delete-site' . $id, 'nonce' );
	if( !isset($_POST['submit']) && !isset($_POST['id']) ) {
		echo '<p>'. __('Confirm that you want to delete the website. If you delete it, all data will be lost. The deletion of the site is irreversible and cannot be recovered.', 'wemcor-multisite') .'</p>';
		echo '<input type="hidden" name="id" value="' . $id . '" />';
		submit_button(__('Delete site', 'wemcor-multisite'));
	}
	echo '</form></div>';
}

add_action( 'network_admin_edit_deleteweb',  'wemcor_delete_web' );
function wemcor_delete_web() {
	$blog_id = $_POST['id'];
	$current_blog_details = get_blog_details( array( 'blog_id' => $blog_id ) );
	$site_name = $current_blog_details->blogname;
	wp_verify_nonce( $_POST['nonce'], 'wemcor-delete-site' . $blog_id);

	//delete site
	wp_delete_site( $blog_id );

	wp_redirect( add_query_arg( array(
		'page' => 'mis-sitios',
		'id' => $blog_id,
		'blogname' => $site_name,
		'delete' => 'true'), admin_url('admin.php')
	));

	exit;
}

// Validaciones
add_action( 'current_screen', 'wemcor_redirects_delete_site' );
function wemcor_redirects_delete_site(){
	$screen = get_current_screen();
	if( $screen->id !== 'sites_page_deletesite-network' ) {
		return;
	}

	$id = isset( $_REQUEST['id'] ) ? intval( $_REQUEST['id'] ) : 0;

	if ( ! $id ) {
		wp_die( __('Invalid site ID.', 'wemcor-multisite') );
	}

	$details = get_site( $id );
	if ( ! $details ) {
		wp_die( __( 'The requested site does not exist.', 'wemcor-multisite' ) );
	}
}

// Esconder menu en barra lateral (ya se muestra en tabs de site info)
add_action( 'admin_head', 'wemcor_hide_delete_site' );
function wemcor_hide_delete_site() {

	echo '<style>
	#menu-site .wp-submenu li:nth-child(4){
		display:none;
	}
	#adminmenu li.toplevel_page_delete-site {
		display: none;
	}
	</style>';
}

