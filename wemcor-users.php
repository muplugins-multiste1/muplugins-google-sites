<?php
/*
Plugin Name: Wemcor Users
Plugin URI:
Description: Sistema de configuración para asignar sites a usuarios y grupos de usuarios por site
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


add_action( 'plugins_loaded', 'wemcor_users_plugin_loaded' );
function wemcor_users_plugin_loaded() {
	assign_sites_to_teachers();
	assign_sites_to_managers();
	if ( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
		require_once plugin_dir_path(__FILE__) . 'includes/table-users.php';
		Wemcor_Users::get_instance();
	}
}

add_action( 'admin_head', 'wemcor_users_admin_head' );
function wemcor_users_admin_head() { ?>
	<style>
	#wemcor-wrap.wemcor-table-users table #assign,
	#wemcor-wrap.wemcor-table-users table .column-assign {
		text-align: center;
	}
	</style>
<?php
}

function assign_sites_to_teachers() {

	if ( DD_DEMO_MODE && defined( 'WP_CLI' ) && WP_CLI ) return false;

	$url = wemcor_get_api_internal_url('users');
	$file = @file_get_contents($url);
	if (!$file) return false;

	$users_json = json_decode($file, true);

	if( is_array($users_json) ) :
		foreach( $users_json as $user_json ) {
			$role = $user_json['role'];
			$user = get_user_by( 'login', $user_json['id'] );
			$password = wp_generate_password( 12, true, true );

			if( $user ) {
				$id_user = $user->ID;
			} else {
				$id_user = wp_create_user( $user_json['id'], $password, $user_json['email'] );
			}

			$to_blog = function_exists('add_user_to_blog') ? add_user_to_blog(1, $id_user, $role) : false;
		}
	endif;
}

function assign_sites_to_managers() {

	if ( DD_DEMO_MODE && defined( 'WP_CLI' ) && WP_CLI ) return false;

	$url = wemcor_get_api_internal_url('users');
	$file = @file_get_contents($url);
	if (!$file) return false;

	$users_json = json_decode($file, true);

	if( is_array($users_json) ) :
		foreach( $users_json as $user_json ) {
			if( $user_json['role'] != 'manager' ) continue;
			$role = $user_json['role'];
			$user = get_user_by( 'login', $user_json['id'] );

			if( $user ) {
				$id_user = $user->ID;
				$all_sites = get_sites();
				foreach ( $all_sites as $site ) {
					$to_blog = function_exists('add_user_to_blog') ? add_user_to_blog($site->blog_id, $id_user, $role) : false;
				}
			}
		}
	endif;
}
