<?php

if( is_multisite() && current_user_can( 'read' ) ) :
	global $wpdb;

	/** Load WordPress Administration Bootstrap */
	require_once ABSPATH . 'wp-admin/admin.php';

	/** WordPress Translation Installation API */
	require_once ABSPATH . 'wp-admin/includes/translation-install.php';

	if ( isset( $_REQUEST['action'] ) && 'add-site' === $_REQUEST['action'] ) {
		check_admin_referer( 'add-blog', '_wpnonce_add-blog' );

		if ( ! is_array( $_POST['blog'] ) ) {
			wp_die( __( 'Can&#8217;t create an empty site.' ) );
		}

		$blog   = $_POST['blog'];
		$domain = '';

		$blog['domain'] = trim( $blog['domain'] );
		if ( preg_match( '|^([a-zA-Z0-9-])+$|', $blog['domain'] ) ) {
			$domain = strtolower( $blog['domain'] );
		}

		// If not a subdomain installation, make sure the domain isn't a reserved word.
		if ( ! is_subdomain_install() ) {
			$subdirectory_reserved_names = get_subdirectory_reserved_names();

			if ( in_array( $domain, $subdirectory_reserved_names, true ) ) {
				wp_die(
					sprintf(
						/* translators: %s: Reserved names list. */
						__( 'The following words are reserved for use by WordPress functions and cannot be used as blog names: %s', 'wemcor-multisite' ),
						'<code>' . implode( '</code>, <code>', $subdirectory_reserved_names ) . '</code>'
					)
				);
			}
		}

		$title = $blog['title'];
		if( empty( $title ) ) {
			wp_die( __( 'Missing name site.', 'wemcor-multisite' ) );
		}

		$meta = array(
			'public' => 1,
		);


		// Handle translation installation for the new site.
		if ( isset( $_POST['WPLANG'] ) ) {
			if ( '' === $_POST['WPLANG'] ) {
				$meta['WPLANG'] = ''; // en_US
			} elseif ( in_array( $_POST['WPLANG'], get_available_languages(), true ) ) {
				$meta['WPLANG'] = $_POST['WPLANG'];
			} elseif ( current_user_can( 'install_languages' ) && wp_can_install_language_pack() ) {
				$language = wp_download_language_pack( wp_unslash( $_POST['WPLANG'] ) );
				if ( $language ) {
					$meta['WPLANG'] = $_POST['WPLANG'];
				}
			}
		}

		if ( empty( $domain ) ) {
			wp_die( __( 'Missing or invalid site address.', 'wemcor-multisite' ) );
		}

		if ( is_subdomain_install() ) {
			$newdomain = $domain . '.' . preg_replace( '|^www\.|', '', get_network()->domain );
			$path      = get_network()->path;
		} else {
			$newdomain = get_network()->domain;
			$path      = get_network()->path . $domain . '/';
		}

		$user = wp_get_current_user();
		$email = $user->user_email;
		if ( isset( $email ) && '' === trim( $email ) ) {
			wp_die( __( 'Missing email address.', 'wemcor-multisite' ) );
		}
		if ( ! is_email( $email ) ) {
			wp_die( __( 'Invalid email address.', 'wemcor-multisite' ) );
		}

		$password = wp_generate_password( 12, false );
		$user_id  = email_exists( $email );

		if ( ! $user_id ) { // Usuario siempre debe existir pues estamos creando desde dentro backend. Solo puede pasar esto si el usuario por alguna razón se manipuló su mail desde base de datos y no dispone de él
			wp_die( __( 'Upps!!! Critical error. Contact with the site administrator', 'wemcor-multisite' ) );
		}



		// super admin id for assign to new blog site
		$super_admin_id = wemcor_get_super_admin();


		$wpdb->hide_errors();
		$id = wpmu_create_blog( $newdomain, $path, $title, $super_admin_id, $meta, get_current_network_id() );
		$wpdb->show_errors();
		if ( ! is_wp_error( $id ) ) {

			// assign user
			if ( ! is_super_admin( $user_id ) && ! get_user_option( 'primary_blog', $user_id ) ) {
				update_user_option( $super_admin_id, 'primary_blog', $id, false );
			}
			$role = wemcor_get_user_site_role($id, $user_id);
			add_user_to_blog($id, $user_id, $role);
			//add_user_to_blog($id, $user_id, 'student');

			// assign owner blog
			update_blog_option( $id, 'owner_user', $user_id );

			// customize settings new site
			switch_to_blog($id);
				$options = [
					'date_format' => 'd/m/Y',
					'time_format' => 'H:i',
					'links_updated_date_format' => 'd/m/Y H:i',
					'timezone_string' => 'Europe/Madrid',
					'template' => 'generatepress',
					'stylesheet' => 'generatepress',
					'show_on_front' => 'page'
				];
				wemcor_set_site_options($id, $options);
				wemcor_delete_all_pages_new_site();

				// update generatepress settings

				if(defined( 'GENERATE_VERSION' )){
					update_option( 'generate_db_version', sanitize_text_field( GENERATE_VERSION ) );
				}

				if(defined( 'GENERATEBLOCKS_VERSION' )){
					update_option( 'generateblocks_version', sanitize_text_field( GENERATEBLOCKS_VERSION ) );
				}

				$gsettings = get_option( 'generate_settings', array() );
				
				$gsettings['layout_setting'] = 'no-sidebar';
				$gsettings['content_background_color'] = 'var(--base-3)';
				$gsettings['background_color'] = 'var(--base-3)';
				$gsettings['content_layout_setting'] = 'one-container';

                update_option( 'generate_settings', $gsettings );

				$post_id = wemcor_create_first_pages($_POST['theme_site']);

				//wemcor_create_widgets($options['template']);
				//wemcor_change_role_user($id, $user_id);
				//wpmu_new_site_admin_notification( $id, $user_id );
				//wpmu_welcome_notification( $id, $user_id, $password, $title, array( 'public' => 1 ) );
				//wemcor_asign_site_to_users($id, $user_id);
			restore_current_blog();


			//redirect to first new page
			if($post_id) {
				//wp_redirect( get_admin_url($id, 'post-new.php?post_type=page') );//http://test.wemcor.es/prueba1/wp-admin/post-new.php?post_type=page
				wp_redirect( get_admin_url($id, 'post.php?post='.$post_id.'&action=edit') );//https://wp.test.digitaldemocratic.net/tema1/wp-admin/post.php?post=3&action=edit
				exit;
			}
		} else {
			wp_die( $id->get_error_message() );
		}
	}

	?>

	<div class="wrap">
		<h1 id="add-new-site"><?php _e( 'Add new site', 'wemcor-multisite' ); ?></h1>
		<?php
		if ( ! empty( $messages ) ) {
			foreach ( $messages as $msg ) {
				echo '<div id="message" class="updated notice is-dismissible"><p>' . $msg . '</p></div>';
			}
		}
		?>
		<p>
		<?php
		printf(
			/* translators: %s: Asterisk symbol (*). */
			__( 'Required fields are marked %s', 'wemcor-multisite' ),
			'<span class="required">*</span>'
		);
		?>
		</p>

		<form method="post" action="<?php echo admin_url( 'admin.php?page=nuevo-sitio&action=add-site' ); ?>" novalidate="novalidate">

			<?php wp_nonce_field( 'add-blog', '_wpnonce_add-blog' ); ?>

			<div class="section-title-choose-theme">
				<p class="label-choose-theme"><?php esc_html_e('Choose Theme', 'wemcor-multisite'); ?></p>
				<!-- <small><?php //esc_html_e('Default theme: Theme 1', 'wemcor-multisite'); ?></small> -->
			</div>
			<div id="images_theme">
			<?php
			$files = list_files( dirname(__FILE__) . '/../screenshots/', 1);
			$i = 0;
			sort($files);
			foreach( $files as $file ) {
				$name = explode("/", $file);
				$name = end($name);//1-twentytwelve.png
				$explode_name = explode('-', $name);
				$end_name = str_replace('.jpg', '', $explode_name[1]);//twentywelve
				//$description = wp_get_theme($end_name)['description'];

				if($i) $style = 'display:none;';
				else $style = '';

				if($i == 0) $class = 'active';
				else $class = '';
				//temporal
				$class = '';

				$i++;

				?>

				<div class="item-image">
					<div class="item-image-wrap <?php echo $class; ?>">
						<?php
						$name_id = str_replace('.jpg', '', $name);
						?>
						<img id="<?php echo $name_id; ?>" class="image-theme" width="220" src="<?php echo plugin_dir_url(dirname(__FILE__)) ?>screenshots/<?php echo $name; ?>">
					</div>
					<div class="item-content-wrap">
						<?php
						$name_pattern = $explode_name;
						unset($name_pattern[0]);
						$name_pattern = ucfirst(str_replace('.jpg', '', implode(' ', $name_pattern)));
						?>
						<h4><?php esc_html_e($name_pattern, 'wemcor-multisite'); ?><span class="tick" style="<?php echo $style; ?>"> ✅</span></h4>

						<!-- <h4><?php //esc_html_e('Theme '.$i, 'wemcor-multisite'); ?></h4> -->
						<!-- <p><?php //esc_html_e('Description Theme '.$i, 'wemcor-multisite'); ?></p> -->

					</div>
				</div>
			<?php } ?>
			</div>
			<div style="clear:both;"></div>

			<table class="form-table" role="presentation">
			<tr class="form-field form-required">
				<th scope="row"><label for="site-address"><?php _e( 'Site Address (URL)', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
				<td>
				<?php if ( is_subdomain_install() ) { ?>
					<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required /><span class="no-break">.<?php echo preg_replace( '|^www\.|', '', get_network()->domain ); ?></span>
					<?php
				} else {
					echo get_network()->domain . get_network()->path
					?>
					<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required />
					<?php
				}
				echo '<p class="description" id="site-address-desc">' . __( 'Only lowercase letters (a-z), numbers, and hyphens are allowed.', 'wemcor-multisite' ) . '</p>';
				?>
				</td>
			</tr>
			<tr class="form-field form-required">
				<th scope="row"><label for="site-title"><?php _e( 'Site Title', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
				<td><input name="blog[title]" type="text" class="regular-text" id="site-title" required /></td>
			</tr>
			<?php
			$languages    = get_available_languages();
			$translations = wp_get_available_translations();

			if ( ! empty( $languages ) || ! empty( $translations ) ) :
				?>
				<tr class="form-field form-required">
					<th scope="row"><label for="site-language"><?php _e( 'Site Language', 'wemcor-multisite' ); ?></label></th>
					<td>
						<?php
						// User language default.
						$lang = get_user_locale();

						if ( ! in_array( $lang, $languages, true ) ) {
							$lang = get_site_option( 'WPLANG' );
						}

						wp_dropdown_languages(
							array(
								'name'                        => 'WPLANG',
								'id'                          => 'site-language',
								'selected'                    => $lang,
								'languages'                   => $languages,
								'translations'                => $translations,
								'show_available_translations' => current_user_can( 'install_languages' ) && wp_can_install_language_pack(),
							)
						);
						?>
					</td>
				</tr>
			<?php endif; // Languages. ?>
				<!--<tr class="form-field">
					<td colspan="2" class="td-full"><p id="site-admin-email"><?php //_e( 'A new user will be created if the above email address is not in the database.', 'wemcor-multisite' ); ?><br /><?php //_e( 'The username and a link to set the password will be mailed to this email address.' ); ?></p></td>
				</tr>-->
				<tr class="form-field form-required">
					<td><input id="theme_site" type="hidden" name="theme_site" value="1-en-blanc"></td>
				</tr>
			</table>

			<?php submit_button( __( 'Add new site', 'wemcor-multisite' ), 'primary', 'add-site' );
			?>
		</form>

	</div><!-- end wrap -->

<?php

endif;


function wemcor_create_first_pages($pattern) {
	$post_content = '';
	if( $pattern != '1-en-blanc') {
		$url = plugin_dir_url(dirname(__FILE__)) . 'templates/templates/' . $pattern .'.json';
		$json = @file_get_contents($url);
		if ($json){
			$json_data = json_decode($json, true);
			$json_data['content'] = str_replace('%%DOMAIN%%', 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/images', $json_data['content']);
			$post_content = $json_data['content'];
		}
	}else{
		$url = plugin_dir_url(dirname(__FILE__)) . 'templates/headers/encabezado.json';
		$json = @file_get_contents($url);
		if ($json){
			$json_data = json_decode($json, true);
			$json_data['content'] = str_replace('%%DOMAIN%%', 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/images', $json_data['content']);
			$post_content = $json_data['content'];
		}
	}

	$postarr = [
		'post_title' => __('Pàgina d\'inici', 'wemcor-multisite'),
		'post_content' => $post_content,
		'post_status' => 'publish',
		'post_type' => 'page'
	];


	$page_on_front = wp_insert_post($postarr, false, false);
	update_option('page_on_front', $page_on_front);

	//create page for blog
	unset($postarr['content']);
	$postarr['post_title'] = __('Blog', 'wemcor-multisite');
	$page_for_posts = wp_insert_post($postarr, false, false);
	update_option('page_for_posts', $page_for_posts);


	//set meta_keys/values to post_meta
	add_post_meta( $page_on_front, '_generate-sidebar-layout-meta', 'no-sidebar' );
	add_post_meta( $page_on_front, '_generate-disable-headline', 'true' );

	if( $pattern != '1-en-blanc') {
		add_post_meta( $page_on_front, '_generate-full-width-content', 'true' );
	}

	update_option('wemcor_pattern', $pattern.'.jpg');

	return $page_on_front;
}
