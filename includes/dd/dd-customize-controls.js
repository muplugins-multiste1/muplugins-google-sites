( function( $ ) {

	wp.domReady(function () {

		var api = wp.customize;

		/**
		 * Customizer control checkbox multiple
		 */
		api.controlConstructor['checkbox-multiple'] = api.Control.extend( {

			ready: function() {
				var control = this;

				control.container.on( 'change', 'input[type="checkbox"]', function() {

					var values = control.container.find( 'input[type="checkbox"]:checked' ).map(
						function() {return this.value; }
					).get().join( ',' );

					control.setting.set( values );

				} );
			}
	
		} );

	});

} )( jQuery );