<?php
//if( is_multisite() && current_user_can( 'read' ) ) :
if( is_multisite() ) :

	// Display redirect notice if available
	// type This can be "info", "warning", "error" or "success", "warning" as default
	$dd_messages = array(
		'import-pending' => array (
			'notice' => __('The site will be available when the import process is complete. This process may take a few minutes.', 'wemcor-multisite'),
			'type' => 'info'
		),
	);
	$dd_msg_id = isset($_GET['msg']) ? sanitize_text_field($_GET['msg']) : NULL;

	if (array_key_exists($dd_msg_id, $dd_messages)) {
		$notice =  $dd_messages[$dd_msg_id];
		printf('<div class="notice notice-%1$s is-dismissible"><p>%2$s</p></div>',
            $notice['type'],
            $notice['notice']
        );
	}


	$user = wp_get_current_user();

	$user_id = get_current_user_id();

	$roles = [];
	foreach( $user->roles as $role ) {
		$roles[] = $role;
	}

	$title = 'Els meus llocs webs';

	if( in_array( 'manager', $roles ) ) $blogs = get_sites();


	else $blogs = get_blogs_of_user( $user_id );

	?>

	<div class="wrap">
		<h1 class="wp-heading-inline">
		<?php
		if( in_array('manager', $roles) ) echo __('All websites', 'wemcor-multisite');
		else echo __('My websites', 'wemcor-multisite');
		echo ' <a href="' . esc_url( admin_url() ) . 'admin.php?page=nuevo-sitio" class="page-title-action">';
		echo __('Add new website', 'wemcor-multisite');
		echo '</a>';
		echo ' <a href="' . esc_url( admin_url() ) . 'admin.php?page=import-gsite" class="page-title-action">';
		echo __('Import website from Google Sites', 'wemcor-multisite');
		echo '</a>';
		if( in_array( 'manager', $roles ) && get_current_blog_id() != BLOG_ID_CURRENT_SITE ) echo '<a class="page-title-action no-border" href=" '. esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) .'admin.php?page=mis-sitios">'. __( 'Return to Main site', 'wemcor-multisite' ) .'</a>';
		?>
		</h1>

		<?php
		if( isset( $_GET['delete'] ) && isset( $_GET['page'] ) && $_GET['page'] == 'mis-sitios' ) {
			echo '<div id="message" class="updated notice is-dismissible">
				<p>'. $_GET['blogname'] .' '. __('Site has deleted successfully', 'wemcor-multisite') .'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">'. __('Dismiss this notice', 'wemcor-multisite').'</span></button>
			</div>';
		}

		//if( count($roles) > 0 ) {
		if( !empty($roles) ) { ?>

			<form id="myblogs" method="post">
				<ul class="my-sites striped">
					<?php
					$blog_id = get_current_blog_id();

					if( in_array( 'manager', $roles ) ) all_sites($blogs, $blog_id);
					else user_sites($blogs, $blog_id, $roles);

					?>
				</ul>
			</form>

		<?php
		} else {
			echo '<p>'. __('You don\'t have any websites assigned to you. Create a new website or', 'wemcor-multisite').' <a href="'. esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) .'admin.php?page=assign-users">'. __( 'assign yourself one', 'wemcor-multisite' ) .'</a></p>';
			echo '<p>'. __('Return to', 'wemcor-multisite').' <a href=" '. esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) .'admin.php?page=mis-sitios">'. __( 'Main site', 'wemcor-multisite' ) .'</a></p>';
		}

		// if ( current_user_can( 'read' ) ) echo '<p style="margin-top: 40px;"><a href="'.admin_url().'admin.php?page=nuevo-sitio" class="button button-primary">'. __('Add new website', 'wemcor-multisite') .'</a></p>';
		?>
	</div>

<?php
endif;

function get_screenshot_stylesheet($stylesheet) {

	if( $stylesheet) return 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/screenshots/'.$stylesheet;

	return false;
}

function all_sites($blogs, $blog_id) {

	if (!function_exists('get_blog_option')){
		return false;
	}

	foreach ( $blogs as $user_blog ) {

		switch_to_blog( $user_blog->blog_id );

		//if( BLOG_ID_CURRENT_SITE == $user_blog->blog_id && get_current_user_id() != 1) continue;
		if( BLOG_ID_CURRENT_SITE == $user_blog->blog_id ) continue;

		$blog_details = get_blog_details($user_blog->blog_id);

		$publish_web = get_blog_option( $user_blog->blog_id, 'wemcor-publishweb' );
		$dd_import_status = get_blog_option( $user_blog->blog_id, 'dd_import_status' );


		$owner = wemcor_get_owner($user_blog->blog_id);

		$property_id = get_blog_option( $user_blog->blog_id, 'owner_user' );
		$property = get_userdata( $property_id );

		$src = get_screenshot_stylesheet(get_blog_option( $user_blog->blog_id, 'wemcor_pattern' ));


		echo ($blog_id == $user_blog->blog_id) ? '<li class="mi-sitio" style="background-color:#e6e6e6;border:1px solid #c8c8c8">' : '<li class="mi-sitio">';


			//image
			echo '<div class="item-mi-sitio-image">';
				if($src) echo '<img style="width:100%;height:auto;" src="'.$src.'"/>';
				else echo '<img style="width:100%;height:auto;" alt="screenshot" src="http://s.wordpress.com/mshots/v1/'.urlencode(home_url()).'"/>';
			echo '</div>';

			//content
			echo '<div class="item-mi-sitio-content">';

				//title + actions
				echo '<div class="item-mi-sitio-title" style="min-width:150px;">';
					echo "<h3>{$blog_details->blogname}</h3>";
					echo ($publish_web) ? '<p><span style="font-size:11px;padding-right:7px;">🟢</span><span>'. __("public site", "wemcor-multisite") .'</span></p>' : '<p><span style="padding-right:7px;">🔒</span><span>'. __("private site", "wemcor-multisite") .'</span></p>';
					echo ($owner) ? '<p><span style="padding-right:7px;">📍</span><span>'. __("Owner site", "wemcor-multisite") .'</span></p>' : '';
					echo (!$owner) ? '<p><span style="padding-right:7px;">👉</span><span>'. __("Property email:", "wemcor-multisite") .' <a href="mailto:'.$property->user_email.'">@'.$property->display_name.'</a></span></p>' : '';
					if($dd_import_status !== 'processing'){
						echo (!$owner) ? '<p><span style="padding-right:7px;padding-left:3px;font-size:9px;">🔁 </span><span>'. '<a href="' . esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) . "admin.php?page=change-property&id=".$user_blog->blog_id.'">' . __( 'Change property', 'wemcor-multisite' ) . '</a></span></p>' : '';
						echo '<p><span style="padding-right:7px;padding-left:3px;font-size:9px;">❌ </span><span>'. '<a style="color:red;vertical-align:middle;" href="' . esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) . "admin.php?page=delete-site&id=".$user_blog->blog_id.'">' . __( 'Delete site', 'wemcor-multisite' ) . '</a></span></p>';
					}
				echo '</div>';

				echo '<div class="item-pages" style="margin-right:50px;">';

					echo '<p style="font-weight:bold;">'. __('Actions', 'wemcor-multisite').'</p>';

					if($dd_import_status !== 'pending' && $dd_import_status !== 'processing' && $dd_import_status !== 'error'){

						$actions = "<a href='" . esc_url( home_url() ) . "'>" . __( 'Visit web', 'wemcor-multisite' ) . '</a>';
						$actions .= "<a href='" . esc_url( admin_url() ) . "edit.php?post_type=page'>" . __( 'All pages', 'wemcor-multisite' ) . '</a>';
						$actions .= "<a href='" . esc_url( admin_url() ) . "edit.php'>" . __( 'All posts', 'wemcor-multisite' ) . '</a>';
						$actions .= "<a href='" . esc_url( admin_url() ) . "customize.php'>" . __( 'Customize site', 'wemcor-multisite' ) . '</a>';

						if( BLOG_ID_CURRENT_SITE != $user_blog->blog_id ) {
							if($publish_web) {
								$actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=publish-web'>" . __( 'Unpublish web', 'wemcor-multisite' ) . '</a>';
							} else {
								$actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=publish-web'>" . __( 'Publish web', 'wemcor-multisite' ) . '</a>';
							}
						}

						$actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=assign-users'>" . __( 'Assign Users', 'wemcor-multisite' ) . '</a>';

					}else{
						$actions = get_import_status($dd_import_status);
					}

					echo '<p class="my-sites-actions">' . $actions . '</p>';

				echo '</div>';

				//pages
				echo '<div class="item-pages">';
				if($dd_import_status !== 'pending' && $dd_import_status !== 'processing' && $dd_import_status !== 'error'){

					$pages = get_pages(['post_status'=>'publish']);
					if($pages) {
							echo '<p style="font-weight:bold;">'. __('Website Pages', 'wemcor-multisite').'</p>';
							foreach($pages as $page) {

								echo '<p>'.$page->post_title.' <a href="'.home_url($page->post_name).'">'. __('Visit', 'wemcor-multisite') .'</a>';
							    if( current_user_can( 'edit_posts' ) ) echo ' | <a href="'.admin_url().'post.php?post='.$page->ID.'&action=edit">'. __('Edit', 'wemcor-multisite') .'</a>';
								echo '</p>';
							}
					}

				}
				echo '</div>';

			echo '</div>';
		echo '</li>';

		restore_current_blog();
	}

	if( empty($blogs) ) {
		echo '<p>'. __('You don\'t have any websites assigned to you. Create a new website or', 'wemcor-multisite').' <a href="'. esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) .'admin.php?page=assign-users">'. __( 'assign yourself one', 'wemcor-multisite' ) .'</a></p>';
	}
}

function user_sites($blogs, $blog_id, $roles) {

	if (!function_exists('get_blog_option')){
		return false;
	}

	foreach ( $blogs as $user_blog ) {

		switch_to_blog( $user_blog->userblog_id );

		if( BLOG_ID_CURRENT_SITE == $user_blog->userblog_id && get_current_user_id() != 1) continue;

		$publish_web = get_blog_option( $user_blog->userblog_id, 'wemcor-publishweb' );
		$dd_import_status = get_blog_option( $user_blog->userblog_id, 'dd_import_status' );


		$owner = wemcor_get_owner($user_blog->userblog_id);

		$property_id = get_blog_option( $user_blog->userblog_id, 'owner_user' );
		$property = get_userdata( $property_id );

		$src = get_screenshot_stylesheet(get_blog_option( $user_blog->userblog_id, 'wemcor_pattern' ));

		echo ($blog_id == $user_blog->userblog_id) ? '<li class="mi-sitio" style="background-color:#e6e6e6;border:1px solid #c8c8c8">' : '<li class="mi-sitio">';

			//image
			echo '<div class="item-mi-sitio-image">';
				if($src) echo '<img style="width:100%;height:auto;" src="'.$src.'"/>';
				else echo '<img style="width:100%;height:auto;" alt="screenshot" src="http://s.wordpress.com/mshots/v1/'.urlencode(home_url()).'"/>';
			echo '</div>';

			//content
			echo '<div class="item-mi-sitio-content">';

				//title + actions
				echo '<div class="item-mi-sitio-title" style="min-width:150px;">';
					echo "<h3>{$user_blog->blogname}</h3>";
					echo ($publish_web) ? '<p><span style="font-size:11px;padding-right:7px;">🟢</span><span>'. __("public site", "wemcor-multisite") .'</span></p>' : '<p><span style="padding-right:7px;">🔒</span><span>'. __("private site", "wemcor-multisite") .'</span></p>';
					echo ($owner) ? '<p><span style="padding-right:7px;">📍</span><span>'. __("owner site", "wemcor-multisite") .'</span></p>' : '';
					if($dd_import_status !== 'processing'){
						echo ($owner) ? '<p><span style="padding-right:7px;padding-left:3px;font-size:9px;">❌ </span><span>'. '<a style="color:red;vertical-align:middle;" href="' . esc_url( get_admin_url(BLOG_ID_CURRENT_SITE) ) . "admin.php?page=delete-site&id=".$user_blog->userblog_id.'">' . __( 'Delete site', 'wemcor-multisite' ) . '</a></span></p>' : '';
					}
					echo (!$owner && $property) ? '<p><span style="padding-right:7px;">👉</span><span>'. __("Property email:", "wemcor-multisite") .' <a href="mailto:'.$property->user_email.'">@'.$property->display_name.'</a></span></p>' : '';
				echo '</div>';

				echo '<div class="item-pages" style="margin-right:50px;">';

					echo '<p style="font-weight:bold;">'. __('Actions', 'wemcor-multisite').'</p>';

					if($dd_import_status !== 'pending' && $dd_import_status !== 'processing' && $dd_import_status !== 'error'){

						$actions = "<a href='" . esc_url( home_url() ) . "'>" . __( 'Visit web', 'wemcor-multisite' ) . '</a>';

						if ( current_user_can( 'edit_posts' ) ) {
							$actions .= "<a href='" . esc_url( admin_url() ) . "edit.php?post_type=page'>" . __( 'All pages', 'wemcor-multisite' ) . '</a>';
							$actions .= "<a href='" . esc_url( admin_url() ) . "edit.php'>" . __( 'All posts', 'wemcor-multisite' ) . '</a>';
							if($owner) $actions .= "<a href='" . esc_url( admin_url() ) . "customize.php'>" . __( 'Customize site', 'wemcor-multisite' ) . '</a>';
							// $actions .= "<a href='" . esc_url( admin_url() ) . "index.php'>" . __( 'View Dashboard', 'wemcor-multisite' ) . '</a>';
							// $actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=nuevo-sitio'>" . __( 'Afegir nou lloc web', 'wemcor-multisite' ) . '</a>';
						}

						if ( in_array( 'teacher', $roles) || in_array('manager', $roles) || in_array('administrator', $roles) ) {

							if( BLOG_ID_CURRENT_SITE != $user_blog->userblog_id ) {
								if($publish_web) {
									if($owner) $actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=publish-web'>" . __( 'Unpublish web', 'wemcor-multisite' ) . '</a>';
								} else {
									if($owner) $actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=publish-web'>" . __( 'Publish web', 'wemcor-multisite' ) . '</a>';
								}
							}

							if($owner) $actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=assign-users'>" . __( 'Assign Users', 'wemcor-multisite' ) . '</a>';
						}
					}else{
						$actions = get_import_status($dd_import_status);
					}

					echo '<p class="my-sites-actions">' . $actions . '</p>';

				echo '</div>';

				// pages
				echo '<div class="item-pages">';
				if($dd_import_status !== 'pending' && $dd_import_status !== 'processing' && $dd_import_status !== 'error'){
					$pages = get_pages(['post_status'=>'publish']);
					if($pages) {
							echo '<p style="font-weight:bold;">'. __('Website Pages', 'wemcor-multisite').'</p>';
							foreach($pages as $page) {
	
								echo '<p>'.$page->post_title.' <a href="'.home_url($page->post_name).'">'. __('Visit', 'wemcor-multisite') .'</a>';
								if( current_user_can( 'edit_posts' ) ) echo ' | <a href="'.admin_url().'post.php?post='.$page->ID.'&action=edit">'. __('Edit', 'wemcor-multisite') .'</a>';
								echo '</p>';
							}
					}
				}
				echo '</div>';


			echo '</div>';
		echo '</li>';

		restore_current_blog();
	}

	if( empty($blogs) ) {
		echo '<p>'. __('You don\'t have any websites assigned to you. Create a website to get started', 'wemcor-multisite') .'</p>';
	}
}



function get_import_status($status){
	$msg = '';
	switch ($status){
		case 'pending':
			$msg = '<p><span style="padding-right:7px;">🔒</span><strong style="color:orange;">'. __("Import status: ", "wemcor-multisite") .  __("Pending", "wemcor-multisite") . '</strong></p>';
			$msg .= '<p><span style="color:orange;">' . __('Import has been requested. This process may take a few minutes.', 'wemcor-multisite')  . '</span></p>';
			break;
		case 'processing':
			$msg = '<p><span style="padding-right:7px;">🔒</span><strong style="color:orange;">'. __("Import status: ", "wemcor-multisite") .  __("Processing", "wemcor-multisite") . '</strong></p>';
			$msg .= '<p><span style="color:orange;">' . __('The site will be available when the import process is complete.', 'wemcor-multisite')  . '</span></p>';
			break;
		case 'success':
			$msg = '<p><span style="padding-right:7px;">🟢</span><strong style="color:green;">'. __("Import status: ", "wemcor-multisite") .  __("Success", "wemcor-multisite") . '</strong></p>';
			$msg .= '<p><span style="color:green;">' . __('Import completed successfully.', 'wemcor-multisite')  . '</span></p>';
			break;
		case 'error':
			$msg = '<p><span style="padding-right:7px;">❗</span><strong style="color:red;">'. __("Import status: ", "wemcor-multisite") .  __("Error", "wemcor-multisite") . '</strong></p>';
			$msg .= '<p><span style="color:red;">' . __('Import failed. The site could not be imported.', 'wemcor-multisite')  . '</span></p>';
			break;
	}
	return $msg;

}