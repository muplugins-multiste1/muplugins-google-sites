<?php

if( is_multisite() && current_user_can( 'read' ) ) :
	global $wpdb;

	/** Load WordPress Administration Bootstrap */
	require_once ABSPATH . 'wp-admin/admin.php';

	/** WordPress Translation Installation API */
	require_once ABSPATH . 'wp-admin/includes/translation-install.php';

	if ( isset( $_REQUEST['action'] ) && 'import-site' === $_REQUEST['action'] ) {
		check_admin_referer( 'import-blog', '_wpnonce_import-blog' );

		$gsite_url = isset( $_POST['gsite_url'] ) ? sanitize_url($_POST['gsite_url'] ) : false;
		if ( ! $gsite_url ) {
			wp_die( __( 'Google Site Address (URL) is required.' ) );
		}


		if ( ! is_array( $_POST['blog'] ) ) {
			wp_die( __( 'Can&#8217;t create an empty site.' ) );
		}

		$blog   = $_POST['blog'];
		$domain = '';

		$blog['domain'] = trim( $blog['domain'] );
		if ( preg_match( '|^([a-zA-Z0-9-])+$|', $blog['domain'] ) ) {
			$domain = strtolower( $blog['domain'] );
		}

		// If not a subdomain installation, make sure the domain isn't a reserved word.
		if ( ! is_subdomain_install() ) {
			$subdirectory_reserved_names = get_subdirectory_reserved_names();

			if ( in_array( $domain, $subdirectory_reserved_names, true ) ) {
				wp_die(
					sprintf(
						/* translators: %s: Reserved names list. */
						__( 'The following words are reserved for use by WordPress functions and cannot be used as blog names: %s', 'wemcor-multisite' ),
						'<code>' . implode( '</code>, <code>', $subdirectory_reserved_names ) . '</code>'
					)
				);
			}
		}

		$title = $blog['title'];
		if( empty( $title ) ) {
			wp_die( __( 'Missing name site.', 'wemcor-multisite' ) );
		}

		$meta = array(
			'public' => 1,
			'dd_import_status' => 'pending',
			'dd_import_url' => $gsite_url,
		);


		// Handle translation installation for the new site.
		if ( isset( $_POST['WPLANG'] ) ) {
			if ( '' === $_POST['WPLANG'] ) {
				$meta['WPLANG'] = ''; // en_US
			} elseif ( in_array( $_POST['WPLANG'], get_available_languages(), true ) ) {
				$meta['WPLANG'] = $_POST['WPLANG'];
			} elseif ( current_user_can( 'install_languages' ) && wp_can_install_language_pack() ) {
				$language = wp_download_language_pack( wp_unslash( $_POST['WPLANG'] ) );
				if ( $language ) {
					$meta['WPLANG'] = $_POST['WPLANG'];
				}
			}
		}

		if ( empty( $domain ) ) {
			wp_die( __( 'Missing or invalid site address.', 'wemcor-multisite' ) );
		}

		if ( is_subdomain_install() ) {
			$newdomain = $domain . '.' . preg_replace( '|^www\.|', '', get_network()->domain );
			$path      = get_network()->path;
		} else {
			$newdomain = get_network()->domain;
			$path      = get_network()->path . $domain . '/';
		}

		$user = wp_get_current_user();
		$email = $user->user_email;
		if ( isset( $email ) && '' === trim( $email ) ) {
			wp_die( __( 'Missing email address.', 'wemcor-multisite' ) );
		}
		if ( ! is_email( $email ) ) {
			wp_die( __( 'Invalid email address.', 'wemcor-multisite' ) );
		}

		$password = wp_generate_password( 12, false );
		$user_id  = email_exists( $email );

		if ( ! $user_id ) { // Usuario siempre debe existir pues estamos creando desde dentro backend. Solo puede pasar esto si el usuario por alguna razón se manipuló su mail desde base de datos y no dispone de él
			wp_die( __( 'Upps!!! Critical error. Contact with the site administrator', 'wemcor-multisite' ) );
		}



		// super admin id for assign to new blog site
		$super_admin_id = wemcor_get_super_admin();


		$wpdb->hide_errors();
		$id = wpmu_create_blog( $newdomain, $path, $title, $super_admin_id, $meta, get_current_network_id() );
		$wpdb->show_errors();
		if ( ! is_wp_error( $id ) ) {

			// assign user
			if ( ! is_super_admin( $user_id ) && ! get_user_option( 'primary_blog', $user_id ) ) {
				update_user_option( $super_admin_id, 'primary_blog', $id, false );
			}
			$role = wemcor_get_user_site_role($id, $user_id);
			add_user_to_blog($id, $user_id, $role);
			//add_user_to_blog($id, $user_id, 'student');

			// assign owner blog
			update_blog_option( $id, 'owner_user', $user_id );

			// customize settings new site
			switch_to_blog($id);

				$local_time  = current_datetime();
				$local_timestamp = $local_time->getTimestamp() + $local_time->getOffset();

				$options = [
					'date_format' => 'd/m/Y',
					'time_format' => 'H:i',
					'links_updated_date_format' => 'd/m/Y H:i',
					'timezone_string' => 'Europe/Madrid',
					'template' => 'generatepress',
					'stylesheet' => 'generatepress',
					'show_on_front' => 'page',
					'dd_import_time' => $local_timestamp
				];

				wemcor_set_site_options($id, $options);
				wemcor_delete_all_pages_new_site();
				update_option('wemcor_pattern', $_POST['theme_site'].'.jpg');

				if(defined( 'GENERATE_VERSION' )){
					update_option( 'generate_db_version', sanitize_text_field( GENERATE_VERSION ) );
				}

				if(defined( 'GENERATEBLOCKS_VERSION' )){
					update_option( 'generateblocks_version', sanitize_text_field( GENERATEBLOCKS_VERSION ) );
				}

				$gsettings = get_option( 'generate_settings', array() );

				$gsettings['layout_setting'] = 'no-sidebar';
				$gsettings['content_background_color'] = 'var(--base-3)';
				$gsettings['background_color'] = 'var(--base-3)';
				$gsettings['content_layout_setting'] = 'one-container';

                update_option( 'generate_settings', $gsettings );


			restore_current_blog();


			//redirect to mis-sitios
			wp_redirect( admin_url('/admin.php?page=mis-sitios&msg=import-pending') );
			exit;

		} else {
			wp_die( $id->get_error_message() );
		}
	}

	?>

	<div class="wrap import-gsite-wrap">
		<h1 id="import-gsite-title"><?php _e( 'Import website from Google Sites', 'wemcor-multisite' ); ?></h1>

		<p><?php _e( 'NOTE: The Google Site must be <strong>Public</strong>.', 'wemcor-multisite' ); ?><br><br></p>
		<?php
		if ( ! empty( $messages ) ) {
			foreach ( $messages as $msg ) {
				echo '<div id="message" class="updated notice is-dismissible"><p>' . $msg . '</p></div>';
			}
		}
		?>
		<p>
		<?php
		printf(
			/* translators: %s: Asterisk symbol (*). */
			__( 'Required fields are marked %s', 'wemcor-multisite' ),
			'<span class="required">*</span>'
		);
		?>
		</p>

		<form method="post" action="<?php echo admin_url( 'admin.php?page=import-gsite&action=import-site' ); ?>" novalidate="novalidate">

			<?php wp_nonce_field( 'import-blog', '_wpnonce_import-blog' ); ?>


			<table class="form-table" role="presentation">
				<tr class="form-field form-required">
					<th scope="row"><label for="gsite_url"><?php _e( 'Google Site Address (URL)', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
					<td>
						<input name="gsite_url" type="text" class="regular-text" id="gsite_url" placeholder="https://" required />
						<p class="description" id="site-address-desc"><?php _e( 'Enter the url of the Google Site you want to import', 'wemcor-multisite' ) ?></p>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label for="site-address"><?php _e( 'Site Address (URL)', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
					<td>
					<?php if ( is_subdomain_install() ) { ?>
						<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required /><span class="no-break">.<?php echo preg_replace( '|^www\.|', '', get_network()->domain ); ?></span>
						<?php
					} else {
						echo get_network()->domain . get_network()->path
						?>
						<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required />
						<?php
					}
					echo '<p class="description" id="site-address-desc">' . __( 'Only lowercase letters (a-z), numbers, and hyphens are allowed.', 'wemcor-multisite' ) . '</p>';
					?>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label for="site-title"><?php _e( 'Site Title', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
					<td><input name="blog[title]" type="text" class="regular-text" id="site-title" required /></td>
				</tr>
				<?php
				$languages    = get_available_languages();
				$translations = wp_get_available_translations();

				if ( ! empty( $languages ) || ! empty( $translations ) ) :
					?>
					<tr class="form-field form-required">
						<th scope="row"><label for="site-language"><?php _e( 'Site Language', 'wemcor-multisite' ); ?></label></th>
						<td>
							<?php
							// User language default.
							$lang = get_user_locale();

							if ( ! in_array( $lang, $languages, true ) ) {
								$lang = get_site_option( 'WPLANG' );
							}

							wp_dropdown_languages(
								array(
									'name'                        => 'WPLANG',
									'id'                          => 'site-language',
									'selected'                    => $lang,
									'languages'                   => $languages,
									'translations'                => $translations,
									'show_available_translations' => current_user_can( 'install_languages' ) && wp_can_install_language_pack(),
								)
							);
							?>
						</td>
					</tr>
				<?php endif; // Languages. ?>
				<!--<tr class="form-field">
					<td colspan="2" class="td-full"><p id="site-admin-email"><?php //_e( 'A new user will be created if the above email address is not in the database.', 'wemcor-multisite' ); ?><br /><?php //_e( 'The username and a link to set the password will be mailed to this email address.' ); ?></p></td>
				</tr>-->
				<tr class="form-field form-required">
					<td><input id="theme_site" type="hidden" name="theme_site" value="1-en-blanc"></td>
				</tr>
			</table>

			<?php submit_button( __( 'Import site', 'wemcor-multisite' ), 'primary', 'import-gsite' );
			?>
		</form>

	</div><!-- end wrap -->

<?php

endif;
